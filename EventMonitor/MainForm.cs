﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WMPLib;

using EventMonitor.Outlook;


//код далёк от идеала, некоторые дублирующиеся строки можно было вынести в отдельные методы, да стоит ли усложнять?..
namespace EventMonitor
{
    //главное окно программы
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private BoxMonitor boxMonitor;  //список всех мониторов
        private SoundPlayerBox playerList = new SoundPlayerBox();  //обработчик звуковых оповещений
        private List<Monitor> selectList;   //список для выборки моиторов из файла
        private Monitor obj;    //текущий монитор
        private EditConnectorForm editorForm;   //форма редактирования монитора
        private EditConnMonForm connMonnEditor; //форма редактирования мониторов соединения
        private int id_index = 1;   //индекс для новых мониторов
        private bool isSaved;   //флаг "файл сохранён"
        private bool isConverted = false;   //флаг "загружен файл старой версии программы"
        private List<ConnMonitor> boxConnMonitor;   //список всех мониторов соединения
        private TelegramNotifier tNotifier; //оповещатель в Телеграм

        //класс, в котором хранятся все вызываемые объекты WindowsMediaPlayer
        class SoundPlayerBox
        {
            static object locker = new object();    //локер для синхронизации потоков
            private List<SoundPlayer> list = new List<SoundPlayer>();

            //метод, вызываемый чистильщиком WindowsMediaPlayer'ов для удаления старых объектов
            public void ClearOld()
            {
                lock (locker)
                {
                    var selectPlayerList = (from player in list where player.creationTime < DateTime.Now.AddMinutes(-1) select player).ToList();
                    foreach (var player in selectPlayerList)
                    {
                        player.wmp.controls.stop();
                        list.Remove(player);
                    }
                }
            }

            //метод, вызываемый вручную для удаления и mute'а текущих объектов WindowsMediaPlayer
            public void Clear()
            {
                lock (locker)
                {
                    foreach (var player in list)
                    {
                        player.wmp.controls.stop();
                    }
                    list.Clear();
                }
            }

            //метод добавления и воспроизведения звука объекта WindowsMediaPlayer
            public void Add(SoundPlayer item, string sound)
            {
                lock (locker)
                {
                    list.Add(item);
                    item.wmp.URL = sound;
                }
            }
        }

        //класс WindowsMediaPlayer + его время создания
        class SoundPlayer
        {
            public WindowsMediaPlayer wmp;
            public DateTime creationTime;
        }

        //класс, хранящий все мониторы
        class BoxMonitor
        {
            public List<Monitor> AllMonitorList = new List<Monitor>();
        }

        //метод загрузки основного окна
        private async void MainForm_Load(object sender, EventArgs e)
        {
            Logger.AddLog("\n\n\nProgram started");

            //загрузка конфигурации мониторов соединений
            if (File.Exists("connmonitors.cfg") && File.ReadAllText("connmonitors.cfg") != "")
            {
                string json = File.ReadAllText("connmonitors.cfg");
                var settings = new JsonSerializerSettings();
                settings.TypeNameHandling = TypeNameHandling.Objects;
                try
                {
                    boxConnMonitor = JsonConvert.DeserializeObject<List<ConnMonitor>>(json, settings);
                    foreach (var connMonitor in boxConnMonitor)
                        connMonitor.mainForm = this;
                    Logger.AddLog("Connection monitors configuration loaded");
                }
                catch
                {
                    boxConnMonitor = new List<ConnMonitor> { new OutlookConnMonitor(this) };
                    Logger.AddLog("ERROR: Failed to load connection monitors configuration. Creating new one");
                }
            }
            else
            {
                boxConnMonitor = new List<ConnMonitor> { new OutlookConnMonitor(this) };
                Logger.AddLog("Connection monitors configuration created");
            }

            foreach (var connMonitor in boxConnMonitor)
                connMonitor.StartMonitoring();

            //загрузка мониторов из последнего открытого файла
            if (File.Exists("filename.cfg") && File.ReadAllText("filename.cfg") != "")
            {
                file_path.Text = File.ReadAllText("filename.cfg");

                if (!LoadFromFile(file_path.Text))
                {
                    file_path.Text = "";
                    isSaved = false;
                    pictureBox_isSaved.Image = Image.FromFile("resources\\icons8-cancel-32.png");
                    boxMonitor = new BoxMonitor();
                    MessageBox.Show($"Failed to load {file_path.Text}");
                    Logger.AddLog("Monitors configuration created");
                }
                else
                {
                    if (isConverted)
                    {
                        file_path.Text += " (OLD_VERSION)";
                        isSaved = false;
                        pictureBox_isSaved.Image = Image.FromFile("resources\\icons8-cancel-32.png");
                        MessageBox.Show("Old version configuration file opened");
                        Logger.AddLog("Old version configuration file opened");
                    }
                    else
                    {
                        isSaved = true;
                        pictureBox_isSaved.Image = Image.FromFile("resources\\icons8-ok-32.png");
                    }
                }
            }
            else
            {
                isSaved = false;
                pictureBox_isSaved.Image = Image.FromFile("resources\\icons8-cancel-32.png");
                boxMonitor = new BoxMonitor();
                Logger.AddLog("Monitors configuration created");
            }

            //загрузка конфигурации оповещателя Телеграм
            if (File.Exists("tNotifier.cfg") && File.ReadAllText("tNotifier.cfg") != "")
            {
                string json = File.ReadAllText("tNotifier.cfg");
                var settings = new JsonSerializerSettings();
                settings.TypeNameHandling = TypeNameHandling.Objects;
                try
                {
                    tNotifier = JsonConvert.DeserializeObject<TelegramNotifier>(json, settings);
                    tNotifier.mainForm = this;

                    //расшифровка пароля прокси
                    foreach (var proxy in tNotifier.proxies)
                    {
                        if (!String.IsNullOrEmpty(proxy.encryptedPass))
                            proxy.pass = XORCipher.Decrypt(proxy.encryptedPass);
                        proxy.SetProxy();
                    }

                    Logger.AddLog("Telegram notificator configuration loaded");
                }
                catch (Exception ex)
                {
                    tNotifier = new TelegramNotifier(this);
                    tNotifier.proxies.Add(new TelegramProxy(true));
                    Logger.AddLog("EREOR: Failed to load Telegram notificator configuration. Creating new one");
                }
                if (tNotifier.status)
                {
                    if (!await tNotifier.StartNotifier())
                        MessageBox.Show($"Failed to start Telegram notificator", "Fail!", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                }
            }
            else
            {
                tNotifier = new TelegramNotifier(this);
                tNotifier.proxies.Add(new TelegramProxy(true));
                Logger.AddLog("Telegram notificator configuration created");
            }

            LoadMonitorList();
            CleanWMPs();
        }

        //метод, вызываемый из монитора соединения, чтобы поменять цвет значка соединения
        public void changeConnPic(ConnMonitor.ConnStatus status, ConnMonitor connMonitor)
        {
            if (status == ConnMonitor.ConnStatus.connected)
            {
                //если статус сменился с disconnected на connected
                if(connMonitor.status == ConnMonitor.ConnStatus.disconnected)
                {
                    EventAction(true, new AlarmMessage(connMonitor.Sound, $"Connection \"{connMonitor.connection.GetType()}\" restored!"));

                    SaveToFile("temp.cfg");

                    //код ниже не работает при конфигурации VS Debug
                    if (LoadFromFile("temp.cfg"))
                    {
                        LoadMonitorList();
                        MessageBox.Show($"Connection \"{connMonitor.connection.GetType()}\" restored!\nIf monitor doesn't work you need to click start/stop some monitor.\nAnd then program reload it all", "Connection restored!", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                    }
                    //код выше не работет при конфигурации VS Debug
                    else
                    {
                        LoadMonitorList();
                        CurrentMonitor.Text = 0.ToString();
                        AllMonitor.Text = 0.ToString();
                        id_index = 1;
                        file_path.Text = "";
                        isSaved = false;
                        pictureBox_isSaved.Image = Image.FromFile("resources\\icons8-cancel-32.png");
                        boxMonitor = new BoxMonitor();
                        listView_messages.Items.Clear();
                        MessageBox.Show($"Connection \"{connMonitor.connection.GetType()}\" restored!\nFailed to reload file. Need to open file manually", "Connection restored!", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                    }
                }
                button_conn_mon_config.Image = Image.FromFile("resources\\photodraw.ru-71140.png");
            }
            else if (status == ConnMonitor.ConnStatus.checking)
                button_conn_mon_config.Image = Image.FromFile("resources\\icons8-microsoft-outlook-32.png");
            else
            {
                button_conn_mon_config.Image = Image.FromFile("resources\\photodraw.ru-33916.png");

                EventAction(true, new AlarmMessage(connMonitor.Sound, $"Connection \"{connMonitor.connection.GetType()}\" lost!"));

                MessageBox.Show($"Connection \"{connMonitor.connection.GetType()}\" lost!", "Connection lost!", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
            }
        }

        //костыльный метод, чтобы почти автоматически (надо кликнуть на start/stop) перезагружать мониторы при восстановлении соединения, автоматически не получается - из другого потока они перезагружаются некорректно
        public void ReloadAllMonitors()
        {
            LoadFromFile("temp.cfg");
            LoadMonitorList();
            MessageBox.Show("Reloaded!");
        }

        //метод, вызываемый из оповещателя Телеграм, чтобы поменять цвет значка соединения с Телеграм
        public void changeTPic(bool status)
        {
            if (status)
            {
                button_tnotification.Image = Image.FromFile("resources\\photodraw.ru-36281.png");
            }
            else
            {
                button_tnotification.Image = Image.FromFile("resources\\photodraw.ru-28657.png");
            }
        }

        //метод отображения мониторов в таблице
        private void LoadMonitorList()
        {
            selectList = (from obj in boxMonitor.AllMonitorList where obj is OutlookMonitor select obj).ToList();
            richTextBox_Name.Text = "";
            richTextBox_Path.Text = "";
            richTextBox_Sound.Text = "";

            dataGrid_Connectors.Rows.Clear();

            for (int i = 0; i < selectList.Count; i++)
            {
                if (String.IsNullOrEmpty(selectList[i].Name))
                    selectList[i].Name = selectList[i].Path;
                dataGrid_Connectors.Rows.Add(selectList[i].Id, null, null, null, selectList[i].Name);
                if (selectList[i].Status)
                {
                    dataGrid_Connectors[1, i].Style.BackColor = Color.Green;
                    dataGrid_Connectors[1, i].Style.ForeColor = Color.Green;
                    dataGrid_Connectors[1, i].Style.SelectionBackColor = Color.Green;
                    dataGrid_Connectors[1, i].Style.SelectionForeColor = Color.Green;
                }
                else
                {
                    dataGrid_Connectors[1, i].Style.BackColor = Color.Red;
                    dataGrid_Connectors[1, i].Style.ForeColor = Color.Red;
                    dataGrid_Connectors[1, i].Style.SelectionBackColor = Color.Red;
                    dataGrid_Connectors[1, i].Style.SelectionForeColor = Color.Red;
                }
                if (selectList[i].sNotification)
                {
                    dataGrid_Connectors[2, i].Value = Image.FromFile("resources\\photodraw.ru-11185.png");
                }
                else
                {
                    dataGrid_Connectors[2, i].Value = Image.FromFile("resources\\photodraw.ru-35292.png");
                }
                if (selectList[i].tNotification)
                {
                    dataGrid_Connectors[3, i].Value = Image.FromFile("resources\\photodraw.ru-36281.png");
                }
                else
                {
                    dataGrid_Connectors[3, i].Value = Image.FromFile("resources\\photodraw.ru-28657.png");
                }
            }

            Logger.AddLog("Monitor list loaded");
        }

        //метод, вызываемый мониторами, при срабатывании его триггера
        public async void EventAction(object obj, AlarmMessage e)
        {
            Logger.AddLog($"Notification: {e.Title} - {e.SubTitle}");

            ListViewItem listViewItem = new ListViewItem();
            listViewItem.Text = $"{DateTime.Now.ToString()} {e.Title}";
            listViewItem.SubItems.Add(e.SubTitle);

            //при конфигурации VS Debug список listView_messages.Items не меняется данным потоком => его изменение заключено в try{...}catch{}
            try
            {
                listView_messages.Items.Add(listViewItem);
                listView_messages.Items[listView_messages.Items.Count - 1].EnsureVisible();
            }
            catch { }

            if (obj is Monitor && ((Monitor)obj).tNotification || !(obj is Monitor))
            {
                if (tNotifier.status)
                {
                    //если уведомить в Телеграм не получилось, пишем уведомление в listView_messages.Items, включаем error sound оповещалки и пытаемся перезапустить оповещалку Телеграма
                    if (!await tNotifier.Notify(e.Title, $"{DateTime.Now.ToString()} {e.Title}\n{e.SubTitle}\n\n{e.Body}") && obj != null)
                    {
                        tNotifier.StopNotifier();
                        if (!await tNotifier.StartNotifier())
                        {
                            //если перезапустить оповещалку Телеграма не получилось, пишем уведомление в listView_messages.Items
                            EventAction(null, new AlarmMessage(tNotifier.sound, $"Telegram notification ERROR", $"While notify about {e.Title}"));
                        }
                        EventAction(null, new AlarmMessage(tNotifier.sound, $"Telegram notification ERROR", $"While notify about {e.Title}"));
                    }
                } 
            }

            if (obj is Monitor && ((Monitor)obj).sNotification || !(obj is Monitor))
            {
                if (!String.IsNullOrEmpty(e.Sound))
                {
                    if (File.Exists(e.Sound))
                        AsyncAction(e);
                    else
                    {
                        EventAction(true, new AlarmMessage(title: $"'{e.Title}' sound notification FAILED!", subtitle: $"File '{e.Sound}' doesn't exist!"));
                        MessageBox.Show($"{e.Title} sound notification FAILED!\nFile {e.Sound} doesn't exist!", "Sound notification FAILED!", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                    }
                }
            }
        }

        //метод воспроизведения звука
        private async void AsyncAction(AlarmMessage e)
        {

            await Task.Run(() =>
            {
                //при конфигурации VS Debug список playerList может не меняться данным потоком => его изменение заключено в try{...}catch{}
                try
                {
                    var player = new SoundPlayer { wmp = new WindowsMediaPlayer(), creationTime = DateTime.Now };
                    playerList.Add(player, e.Sound);
                    Logger.AddLog($"Sound notifying about {e.Title}");
                }
                catch{ }
            });
        }

        //метод, который чистит список объектов WindowsMediaPlayer
        private async void CleanWMPs()
        {
            Logger.AddLog("Sound notification queue cleaner started");
            await Task.Run(() => 
            {
                while (true) 
                {
                    Thread.Sleep(150000);
                    playerList.ClearOld();
                    Logger.AddLog("Sound notification queue cleared by cleaner");
                }
            });
        }

        //метод загрузки из файла
        private bool LoadFromFile(string filename)
        {
            if (File.Exists(filename) && File.ReadAllText(filename) != "")
            {
                string json = File.ReadAllText(filename);
                var settings = new JsonSerializerSettings();
                settings.TypeNameHandling = TypeNameHandling.Objects;
                try
                {
                    boxMonitor = JsonConvert.DeserializeObject<BoxMonitor>(json, settings);

                    AllMonitor.Text = boxMonitor.AllMonitorList.Count.ToString();
                }
                catch (JsonSerializationException)
                {
                    //чтение старой версии конфигурационного файла
                    json = json.Replace("EventMonitor1._2", "EventMonitor");
                    json = json.Replace("EventMonitor1.2", "EventMonitor");
                    json = json.Replace(".OutlookConnection", ".Outlook.OutlookConnection");
                    json = json.Replace(".OutlookMonitor", ".Outlook.OutlookMonitor");
                    try
                    {
                        boxMonitor = JsonConvert.DeserializeObject<BoxMonitor>(json, settings);
                        isConverted = true;
                    }
                    catch
                    {
                        Logger.AddLog($"ERROR: Monitor configuration failed to load from the {filename}");
                        return false;
                    }
                }
                catch
                {
                    Logger.AddLog($"ERROR: Monitor configuration failed to load from the {filename}");
                    return false;
                }

                int ready_counter_conn = 0;

                foreach (Monitor item in boxMonitor.AllMonitorList)
                {
                    if (item is OutlookMonitor)
                    {
                        List<ConnMonitor> selectOutlookConnMonList = (from connMon in boxConnMonitor where connMon is OutlookConnMonitor select connMon).ToList();
                        if (selectOutlookConnMonList.First().connection == null)
                            selectOutlookConnMonList.First().StartMonitoring();
                    }
                    item.Id = id_index;
                    id_index++;

                    if (!item.Establish_Connection())
                        continue;
                    item.EventMessage += EventAction;
                    if (item.Status)
                        item.Start();
                    if (item.Status)
                        ready_counter_conn++;
                }

                CurrentMonitor.Text = ready_counter_conn.ToString();

                Logger.AddLog($"Monitor configuration loaded from the {filename}");
                return true;
            }
            else
            {
                Logger.AddLog($"ERROR: Monitor configuration failed to load from the {filename}");
                return false;
            }
        }

        //метод сохранения в файл
        private void SaveToFile(string filename)
        {
            var settings = new JsonSerializerSettings();
            settings.TypeNameHandling = TypeNameHandling.Objects;

            string json = JsonConvert.SerializeObject(boxMonitor, Formatting.Indented, settings);

            if (!File.Exists(filename))
            {
                File.AppendAllText(filename, "");
            }
            File.WriteAllText(filename, json);

            Logger.AddLog($"Monitor configuration saved to the {filename}");
            isConverted = false;
        }

        //метод закрытия основного окна
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!isSaved && (!String.IsNullOrEmpty(file_path.Text) || boxMonitor.AllMonitorList.Any()))
            {
                DialogResult result = MessageBox.Show("Would you like to save current configuration?", "Save?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);

                if (result == DialogResult.Yes)
                {
                    if (String.IsNullOrEmpty(file_path.Text))
                    {
                        SaveFileDialog SFD = new SaveFileDialog();
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            file_path.Text = SFD.FileName;
                        }
                        else
                        {
                            e.Cancel = true;
                            return;
                        }
                    }
                    SaveToFile(file_path.Text);
                }
                else if(result == DialogResult.Cancel)
                {
                    e.Cancel = true;
                    return;
                }
            }

            //сохранение конфигурации оповещателя Телеграм
            if (!File.Exists("tNotifier.cfg"))
            {
                File.AppendAllText("tNotifier.cfg", "");
            }
            foreach(var proxy in tNotifier.proxies)
            {
                if (!String.IsNullOrEmpty(proxy.pass))
                    proxy.encryptedPass = XORCipher.Encrypt(proxy.pass);
            }
            var settings = new JsonSerializerSettings();
            settings.TypeNameHandling = TypeNameHandling.Objects;
            string json = JsonConvert.SerializeObject(tNotifier, Formatting.Indented, settings);
            File.WriteAllText("tNotifier.cfg", json);
            Logger.AddLog("Telegram notificatior configuration saved");

            //сохранение пути последнего открытого файла
            if (!File.Exists("filename.cfg"))
            {
                File.AppendAllText("filename.cfg", "");
            }
            File.WriteAllText("filename.cfg", file_path.Text.Replace(" (OLD_VERSION)",""));

            //сохранение конфигурации монитора подключений
            if (!File.Exists("connmonitors.cfg"))
            {
                File.AppendAllText("connmonitors.cfg", "");
            }
            settings = new JsonSerializerSettings();
            settings.TypeNameHandling = TypeNameHandling.Objects;
            json = JsonConvert.SerializeObject(boxConnMonitor, Formatting.Indented, settings);
            File.WriteAllText("connmonitors.cfg", json);
            Logger.AddLog("Connection monitor configuration saved");
            Logger.AddLog("Program stopped\n");
        }

        //метод обработки нажатия на таблицу мониторов
        private void dataGrid_Connectors_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //при нажатии на шапку
            if (e.RowIndex < 0)
                return;
            else
            {
                obj = selectList.Find(x => x.Id == (int)dataGrid_Connectors[0, e.RowIndex].Value);

                switch (e.ColumnIndex)
                {
                    //включение/выключение монитора
                    case 1: 
                        if (!obj.Status)
                        {
                            try
                            {
                                obj.Start();
                            }
                            catch(Exception ex)
                            {
                                if (ex.Message == "Need to restart")
                                    ReloadAllMonitors();
                                else
                                    MessageBox.Show(ex.Message);
                                return;
                            }

                            if (obj.Status)
                            {
                                CurrentMonitor.Text = (int.Parse(CurrentMonitor.Text) + 1).ToString();

                                LoadMonitorList();
                            }
                        }
                        else
                        {
                            try
                            {
                                obj.Stop();
                            }
                            catch(Exception ex)
                            {
                                if (ex.Message == "Need to restart")
                                    ReloadAllMonitors();
                                else
                                    MessageBox.Show(ex.Message);
                                return;
                            }

                            if (!obj.Status)
                            {
                                CurrentMonitor.Text = (int.Parse(CurrentMonitor.Text) - 1).ToString();

                                LoadMonitorList();
                            }
                        }
                        
                        if(obj.Status)
                            Start_Stop_but.Image = Image.FromFile("resources\\icons8-deaf-32.png");
                        else
                            Start_Stop_but.Image = Image.FromFile("resources\\icons8-hearing-32.png");

                        isSaved = false;
                        pictureBox_isSaved.Image = Image.FromFile("resources\\icons8-cancel-32.png");
                        break;

                    //вкл/выкл звуковых уведомлений
                    case 2:
                        if (obj.sNotification)
                        {
                            obj.sNotification = false;
                            Logger.AddLog($"{obj.ToString()} {obj.Name} sound Notification stopped");
                        }
                        else
                        {
                            obj.sNotification = true;
                            Logger.AddLog($"{obj.ToString()} {obj.Name} sound Notification started");
                        }
                        LoadMonitorList();

                        isSaved = false;
                        pictureBox_isSaved.Image = Image.FromFile("resources\\icons8-cancel-32.png");
                        break;

                    //вкл/выкл уведомлений в Телеграм
                    case 3:
                        if (obj.tNotification)
                        {
                            obj.tNotification = false;
                            Logger.AddLog($"{obj.ToString()} {obj.Name} Telegram Notification stopped");
                        }
                        else
                        {
                            obj.tNotification = true;
                            Logger.AddLog($"{obj.ToString()} {obj.Name} Telegram Notification started");
                        }
                        LoadMonitorList();

                        isSaved = false;
                        pictureBox_isSaved.Image = Image.FromFile("resources\\icons8-cancel-32.png");
                        break;

                    //выделение монитора
                    case 4:
                        break;

                    //воспроизведение звука монитора
                    case 5:
                        AsyncAction(new AlarmMessage(obj.Sound));
                        break;

                    //удаление монитора
                    case 6:
                        dataGrid_Connectors.Rows.RemoveAt(e.RowIndex);
                        boxMonitor.AllMonitorList.Remove(obj);
                        if (int.Parse(AllMonitor.Text) - 1 > -1)
                        {
                            if (obj.Status)
                            {
                                obj.Stop();
                                CurrentMonitor.Text = (int.Parse(CurrentMonitor.Text) - 1).ToString();
                            }
                            AllMonitor.Text = (int.Parse(AllMonitor.Text) - 1).ToString();
                        }

                        Logger.AddLog($"{obj.ToString()} {obj.Name} deleted");

                        isSaved = false;
                        pictureBox_isSaved.Image = Image.FromFile("resources\\icons8-cancel-32.png");
                        break;
                }

                richTextBox_Name.Text = obj.Name;
                richTextBox_Path.Text = obj.Path;
                richTextBox_Sound.Text = obj.Sound;
            }
        }

        //метод обработки двойного нажатия - редактирование монитора
        private void dataGrid_Connectors_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            var oldStatus = obj.Status;
            var oldName = obj.Name;

            editorForm = new EditOutlookConnectorForm((OutlookMonitor)obj);
            editorForm.ShowDialog();

            if (editorForm.isOk)
            {
                if (obj.Status && !oldStatus)
                {
                    obj.Start();
                    if (obj.Status)
                        CurrentMonitor.Text = (int.Parse(CurrentMonitor.Text) + 1).ToString();
                }
                else if (!obj.Status && oldStatus)
                {
                    obj.Stop();
                    if (!obj.Status)
                        CurrentMonitor.Text = (int.Parse(CurrentMonitor.Text) - 1).ToString();
                }

                isSaved = false;
                pictureBox_isSaved.Image = Image.FromFile("resources\\icons8-cancel-32.png");

                Logger.AddLog($"{editorForm.monitor.ToString()} {oldName} edited with new parameters: name: {editorForm.monitor.Name}, path: {editorForm.monitor.Path}, sound: {editorForm.monitor.Sound}, started: {editorForm.monitor.Status.ToString()}, sound notification: {editorForm.monitor.sNotification.ToString()}, Telegram Notification: {editorForm.monitor.tNotification.ToString()}");
            }

            LoadMonitorList();
        }

        //выключение звуков оповещения
        private void Sound_off_butt_Click(object sender, EventArgs e)
        {
            //при конфигурации VS Debug список playerList может не меняться данным потоком => его изменение заключено в try{...}catch{}
            try
            {
                playerList.Clear();
                Logger.AddLog("Sound notification queue cleared by user");
            }
            catch { }
        }

        //создание нового списка мониторов
        private void New_but_Click(object sender, EventArgs e)
        {
            if (!isSaved && (!String.IsNullOrEmpty(file_path.Text) || boxMonitor.AllMonitorList.Any()))
            {
                DialogResult result = MessageBox.Show("Would you like to save current configuration?", "Save?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);

                if (result == DialogResult.Yes)
                {
                    if (String.IsNullOrEmpty(file_path.Text))
                    {
                        SaveFileDialog SFD = new SaveFileDialog();
                        SFD.Filter = "(*.cfg)|*.cfg";
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            file_path.Text = SFD.FileName;
                            SaveToFile(file_path.Text);
                            isSaved = true;
                            pictureBox_isSaved.Image = Image.FromFile("resources\\icons8-ok-32.png");
                        }
                    }
                }
                else if (result == DialogResult.Cancel)
                {
                    return;
                }
            }


            foreach (var monitor in boxMonitor.AllMonitorList)
                monitor.Stop();
            CurrentMonitor.Text = 0.ToString();
            AllMonitor.Text = 0.ToString();
            id_index = 1;
            file_path.Text = "";
            isSaved = false;
            pictureBox_isSaved.Image = Image.FromFile("resources\\icons8-cancel-32.png");
            boxMonitor = new BoxMonitor();
            //при конфигурации VS Debug список playerList может не меняться данным потоком => его изменение заключено в try{...}catch{}
            try
            {
                playerList.Clear();
                Logger.AddLog("Sound notification queue cleared by user");
            }
            catch { }
            listView_messages.Items.Clear();

            Logger.AddLog("New monitors configuration created");

            LoadMonitorList();
        }

        //открытие существующего списка мониторов
        private void Open_but_Click(object sender, EventArgs e)
        {
            if (!isSaved && (!String.IsNullOrEmpty(file_path.Text) || boxMonitor.AllMonitorList.Any()))
            {
                DialogResult result = MessageBox.Show("Would you like to save current configuration?", "Save?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);

                if (result == DialogResult.Yes)
                {
                    if (String.IsNullOrEmpty(file_path.Text))
                    {
                        SaveFileDialog SFD = new SaveFileDialog();
                        SFD.Filter = "(*.cfg)|*.cfg";
                        if (SFD.ShowDialog() == DialogResult.OK)
                        {
                            file_path.Text = SFD.FileName;
                            SaveToFile(file_path.Text);
                            isSaved = true;
                            pictureBox_isSaved.Image = Image.FromFile("resources\\icons8-ok-32.png");
                        }
                    }
                }
                else if (result == DialogResult.Cancel)
                {
                    return;
                }
            }

            OpenFileDialog OFD = new OpenFileDialog();
            OFD.Multiselect = false;
            OFD.Filter = "(*.cfg)|*.cfg|(*.txt)|*.txt|(*.*)|*.*";
            if (OFD.ShowDialog() == DialogResult.OK)
            {
                var oldBoxMonitor = boxMonitor;

                isConverted = false;
                if (LoadFromFile(OFD.FileName))
                {
                    LoadMonitorList();
                    file_path.Text = OFD.FileName;
                    if (isConverted)
                    {
                        file_path.Text += " (OLD_VERSION)";
                        isSaved = false;
                        pictureBox_isSaved.Image = Image.FromFile("resources\\icons8-cancel-32.png");
                        MessageBox.Show("Old version configuration file opened!");
                        Logger.AddLog("Old version configuration file opened!");
                    }
                    else
                    {
                        isSaved = true;
                        pictureBox_isSaved.Image = Image.FromFile("resources\\icons8-ok-32.png");
                    }

                    foreach (var monitor in oldBoxMonitor.AllMonitorList)
                        monitor.Stop();

                    //при конфигурации VS Debug список playerList может не меняться данным потоком => его изменение заключено в try{...}catch{}
                    try
                    {
                        playerList.Clear();
                        Logger.AddLog("Sound notification queue cleared by user");
                    }
                    catch { }
                    listView_messages.Items.Clear();
                }
                else
                {
                    foreach (var monitor in boxMonitor.AllMonitorList)
                        monitor.Stop();
                    boxMonitor = oldBoxMonitor;
                    MessageBox.Show($"Failed to open {OFD.FileName}");
                    Logger.AddLog("New monitors configuration created");
                }
            }
        }

        //сохранение списка мониторов
        private void Save_but_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(file_path.Text) || isConverted)
            {
                SaveFileDialog SFD = new SaveFileDialog();
                SFD.Filter = "(*.cfg)|*.cfg";
                if (SFD.ShowDialog() == DialogResult.OK)
                {
                    file_path.Text = SFD.FileName;
                    SaveToFile(file_path.Text);
                    isSaved = true;
                    pictureBox_isSaved.Image = Image.FromFile("resources\\icons8-ok-32.png");
                }
            }
            else
            {
                SaveToFile(file_path.Text);
                isSaved = true;
                pictureBox_isSaved.Image = Image.FromFile("resources\\icons8-ok-32.png");
            }
        }

        //сохранение как списка мониторов
        private void Save_As_but_Click(object sender, EventArgs e)
        {
            SaveFileDialog SFD = new SaveFileDialog();
            SFD.Filter = "(*.cfg)|*.cfg";
            if (SFD.ShowDialog() == DialogResult.OK)
            {
                file_path.Text = SFD.FileName;
                SaveToFile(file_path.Text);
                isSaved = true;
                pictureBox_isSaved.Image = Image.FromFile("resources\\icons8-ok-32.png");
            }
        }

        //очистка логов
        private void Clear_logs_butt_Click(object sender, EventArgs e)
        {
            //при конфигурации VS Debug список playerList может не меняться данным потоком => его изменение заключено в try{...}catch{}
            try
            {
                playerList.Clear();
                Logger.AddLog("Sound notification queue cleared by user");
            }
            catch { }
            listView_messages.Items.Clear();
        }

        //открытие папки, содержащей звук
        private void Open_sound_folder_but_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(richTextBox_Sound.Text))
            {
                if (File.Exists(richTextBox_Sound.Text))
                    Process.Start("explorer.exe", richTextBox_Sound.Text.Substring(0, richTextBox_Sound.Text.LastIndexOf(@"\")));
                else
                {
                    MessageBox.Show($"File {richTextBox_Sound.Text} doesn't exist!", "File NOT FOUND!", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                }
            }
        }

        //создание нового монитора
        private void Button_new_connector_Click(object sender, EventArgs e)
        {
            editorForm = new EditOutlookConnectorForm();
            editorForm.ShowDialog();
            if (editorForm.isOk)
            {
                editorForm.monitor.Id = id_index;
                id_index++;

                editorForm.monitor.Establish_Connection();
                editorForm.monitor.EventMessage += EventAction;
                if (editorForm.monitor.Status)
                    editorForm.monitor.Start();
                else
                    editorForm.monitor.Stop();

                boxMonitor.AllMonitorList.Add(editorForm.monitor);

                LoadMonitorList();

                if (editorForm.monitor.Status)
                    CurrentMonitor.Text = (int.Parse(CurrentMonitor.Text) + 1).ToString();
                AllMonitor.Text = (int.Parse(AllMonitor.Text) + 1).ToString();


                isSaved = false;
                pictureBox_isSaved.Image = Image.FromFile("resources\\icons8-cancel-32.png");

                Logger.AddLog($"{editorForm.monitor.ToString()} created with parameters: name: {editorForm.monitor.Name}, path: {editorForm.monitor.Path}, sound: {editorForm.monitor.Sound}, started: {editorForm.monitor.Status.ToString()}, sound notification: {editorForm.monitor.sNotification.ToString()}, Telegram Notification: {editorForm.monitor.tNotification.ToString()}");
            }
        }

        //запуск всех мониторов
        private void Button_start_all_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < boxMonitor.AllMonitorList.Count; i++)
            {
                if (!boxMonitor.AllMonitorList[i].Status)
                {
                    boxMonitor.AllMonitorList[i].Start();
                    if (boxMonitor.AllMonitorList[i].Status)
                    {
                        CurrentMonitor.Text = (int.Parse(CurrentMonitor.Text) + 1).ToString();
                        isSaved = false;
                        pictureBox_isSaved.Image = Image.FromFile("resources\\icons8-cancel-32.png");
                    }
                }
            }
            LoadMonitorList();
        }

        //остановка всех мониторов
        private void Button_stop_all_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < boxMonitor.AllMonitorList.Count; i++)
            {
                if (boxMonitor.AllMonitorList[i].Status)
                {
                    boxMonitor.AllMonitorList[i].Stop();
                    if (!boxMonitor.AllMonitorList[i].Status)
                    {
                        CurrentMonitor.Text = (int.Parse(CurrentMonitor.Text) - 1).ToString();
                        isSaved = false;
                        pictureBox_isSaved.Image = Image.FromFile("resources\\icons8-cancel-32.png");
                    }
                }
            }
            LoadMonitorList();
        }

        //инвертация всех мониторов
        private void Button_rotate_status_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < boxMonitor.AllMonitorList.Count; i++)
            {
                if (boxMonitor.AllMonitorList[i].Status)
                {
                    boxMonitor.AllMonitorList[i].Stop();
                    if (!boxMonitor.AllMonitorList[i].Status)
                    {
                        CurrentMonitor.Text = (int.Parse(CurrentMonitor.Text) - 1).ToString();
                        isSaved = false;
                        pictureBox_isSaved.Image = Image.FromFile("resources\\icons8-cancel-32.png");
                    }
                }
                else
                {
                    boxMonitor.AllMonitorList[i].Start();
                    if (boxMonitor.AllMonitorList[i].Status)
                    {
                        CurrentMonitor.Text = (int.Parse(CurrentMonitor.Text) + 1).ToString();
                        isSaved = false;
                        pictureBox_isSaved.Image = Image.FromFile("resources\\icons8-cancel-32.png");
                    }
                }
            }
            LoadMonitorList();
        }

        //удаление всех мониторов
        private void Button_delete_all_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Would you like to delete all monitors?", "Delete all?", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
            if (result == DialogResult.Yes)
            {
                dataGrid_Connectors.Rows.Clear();
                foreach (var monitor in boxMonitor.AllMonitorList)
                    monitor.Stop();
                boxMonitor.AllMonitorList.Clear();
                CurrentMonitor.Text = 0.ToString();
                AllMonitor.Text = 0.ToString();
                richTextBox_Name.Text = "";
                richTextBox_Path.Text = "";
                richTextBox_Sound.Text = "";
                isSaved = false;
                pictureBox_isSaved.Image = Image.FromFile("resources\\icons8-cancel-32.png");

                Logger.AddLog("All monitors deleted!");
            }
        }

        //запуск/остановка выделенного монитора
        private void Start_Stop_but_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(richTextBox_Name.Text) && !String.IsNullOrEmpty(richTextBox_Path.Text) && !String.IsNullOrEmpty(richTextBox_Sound.Text) && obj != null)
            {
                if (obj.Status)
                {
                    try
                    {
                        obj.Stop();
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "Need to restart")
                            ReloadAllMonitors();
                        else
                            MessageBox.Show(ex.Message);
                        return;
                    }
                    if (!obj.Status)
                    {
                        LoadMonitorList();
                        CurrentMonitor.Text = (int.Parse(CurrentMonitor.Text) - 1).ToString();
                        isSaved = false;
                        pictureBox_isSaved.Image = Image.FromFile("resources\\icons8-cancel-32.png");
                        Start_Stop_but.Image = Image.FromFile("resources\\icons8-hearing-32.png");
                    }
                }
                else
                {
                    try
                    {
                        obj.Start();
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message == "Need to restart")
                            ReloadAllMonitors();
                        else
                            MessageBox.Show(ex.Message);
                        return;
                    }
                    if (obj.Status)
                    {
                        LoadMonitorList();
                        CurrentMonitor.Text = (int.Parse(CurrentMonitor.Text) + 1).ToString();
                        isSaved = false;
                        pictureBox_isSaved.Image = Image.FromFile("resources\\icons8-cancel-32.png");
                        Start_Stop_but.Image = Image.FromFile("resources\\icons8-deaf-32.png");
                    }
                }

                richTextBox_Name.Text = obj.Name;
                richTextBox_Path.Text = obj.Path;
                richTextBox_Sound.Text = obj.Sound;
                if (obj.Status)
                    Start_Stop_but.Image = Image.FromFile("resources\\icons8-deaf-32.png");
                else
                    Start_Stop_but.Image = Image.FromFile("resources\\icons8-hearing-32.png");
            }
        }

        //редактирование выделенного монитора
        private void Edit_but_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(richTextBox_Name.Text) && !String.IsNullOrEmpty(richTextBox_Path.Text) && !String.IsNullOrEmpty(richTextBox_Sound.Text) && obj != null)
            {
                var oldStatus = obj.Status;
                var oldName = obj.Name;

                editorForm = new EditOutlookConnectorForm((OutlookMonitor)obj);
                editorForm.ShowDialog();
                if (editorForm.isOk)
                {
                    obj = editorForm.monitor;
                    if (obj.Status && !oldStatus)
                    {
                        obj.Start();
                        if (obj.Status)
                            CurrentMonitor.Text = (int.Parse(CurrentMonitor.Text) + 1).ToString();
                    }
                    else if (!obj.Status && oldStatus)
                    {
                        obj.Stop();
                        if (!obj.Status)
                            CurrentMonitor.Text = (int.Parse(CurrentMonitor.Text) - 1).ToString();
                    }

                    isSaved = false;
                    pictureBox_isSaved.Image = Image.FromFile("resources\\icons8-cancel-32.png");
                    Logger.AddLog($"{editorForm.monitor.ToString()} {oldName} edited with new parameters: name: {editorForm.monitor.Name}, path: {editorForm.monitor.Path}, sound: {editorForm.monitor.Sound}, started: {editorForm.monitor.Status.ToString()}, sound notification: {editorForm.monitor.sNotification.ToString()}, Telegram Notification: {editorForm.monitor.tNotification.ToString()}");
                }

                LoadMonitorList();

                richTextBox_Name.Text = obj.Name;
                richTextBox_Path.Text = obj.Path;
                richTextBox_Sound.Text = obj.Sound;
                if (obj.Status)
                    Start_Stop_but.Image = Image.FromFile("resources\\icons8-deaf-32.png");
                else
                    Start_Stop_but.Image = Image.FromFile("resources\\icons8-hearing-32.png");
            }
        }

        //удаление выделенного монитора
        private void Delete_but_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(richTextBox_Name.Text) && !String.IsNullOrEmpty(richTextBox_Path.Text) && !String.IsNullOrEmpty(richTextBox_Sound.Text) && obj != null)
            {
                boxMonitor.AllMonitorList.Remove(obj);
                if (int.Parse(AllMonitor.Text) - 1 > -1)
                {
                    if (obj.Status)
                    {
                        obj.Stop();
                        CurrentMonitor.Text = (int.Parse(CurrentMonitor.Text) - 1).ToString();
                    }
                    AllMonitor.Text = (int.Parse(AllMonitor.Text) - 1).ToString();
                }
                LoadMonitorList();

                Logger.AddLog($"{obj.ToString()} {obj.Name} deleted");
                isSaved = false;
                pictureBox_isSaved.Image = Image.FromFile("resources\\icons8-cancel-32.png");

                richTextBox_Name.Text = "";
                richTextBox_Path.Text = "";
                richTextBox_Sound.Text = "";
            }
        }

        //редактирование мониторинга соединения
        private void Button_conn_mon_config_Click(object sender, EventArgs e)
        {
            connMonnEditor = new EditConnMonForm(boxConnMonitor.First());
            connMonnEditor.ShowDialog();
        }

        //редактирование оповещателя Телеграм
        private void Button_tnotification_Click(object sender, EventArgs e)
        {
            var editTNotidicatorForm = new EditTNotificatorForm(tNotifier);
            editTNotidicatorForm.ShowDialog();
        }
    }
}
