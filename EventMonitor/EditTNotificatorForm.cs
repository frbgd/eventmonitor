﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Telegram.Bot;
using WMPLib;

namespace EventMonitor
{
    //класс формы настройки оповещалки Телеграм
    public partial class EditTNotificatorForm : Form
    {
        EditTProxyForm editTProxy;
        TelegramNotifier tNotifier;
        TelegramBotClient botClient;
        private WindowsMediaPlayer wmp = new WindowsMediaPlayer();
        public EditTNotificatorForm(TelegramNotifier tNotifier)
        {
            InitializeComponent();
            this.tNotifier = tNotifier;
            textBox_botToken.Text = this.tNotifier.botToken;
            if(this.tNotifier.chatId != 0)
                textBox_chatId.Text = this.tNotifier.chatId.ToString();
            foreach (var tProxy in this.tNotifier.proxies)
            {
                comboBox_proxies.Items.Add(tProxy);
                if (tProxy.isCurrent)
                    comboBox_proxies.SelectedIndex = comboBox_proxies.Items.Count - 1;
            }
            textBox_sound.Text = this.tNotifier.sound;
            checkBox_enabled.Checked = this.tNotifier.status;
        }

        private async void Button_test_Click(object sender, EventArgs e)
        {
            if (button_test.Text != "Processing...")
            {
                if (!String.IsNullOrEmpty(textBox_botToken.Text))
                {
                    int chatId;
                    try
                    {
                        chatId = Convert.ToInt32(textBox_chatId.Text);
                    }
                    catch
                    {
                        MessageBox.Show("Invalid Chat Id!");
                        return;
                    }
                    button_test.Text = "Processing...";
                    var proxy = (TelegramProxy)comboBox_proxies.Items[comboBox_proxies.SelectedIndex];
                    try
                    {   
                        if (!String.IsNullOrEmpty(proxy.host))
                        {
                            botClient = new TelegramBotClient(textBox_botToken.Text, proxy.GetProxy());
                        }
                        else
                            botClient = new TelegramBotClient(textBox_botToken.Text);
                        await botClient.GetMeAsync();
                        await botClient.SendTextMessageAsync(chatId, "test");
                        MessageBox.Show("OK");
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show($"ERROR\n{ex.Message}", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                        if (textBox_botToken.Text == tNotifier.botToken && Convert.ToInt32(textBox_chatId.Text) == tNotifier.chatId && proxy.isCurrent)
                        {
                            tNotifier.StopNotifier();
                            checkBox_enabled.Checked = false;
                        }
                    }
                    button_test.Text = "Test";
                }
                else
                    MessageBox.Show("Bot token can't be empty!");
            }
            else
                MessageBox.Show("Previously test is running!");
        }

        private void Button_proxy_Click(object sender, EventArgs e)
        {
            editTProxy = new EditTProxyForm(tNotifier);
            editTProxy.ShowDialog();
            comboBox_proxies.Items.Clear();
            foreach (var tProxy in tNotifier.proxies)
            {
                comboBox_proxies.Items.Add(tProxy);
            }
            comboBox_proxies.SelectedIndex = 0;
        }

        private async void Button_apply_Click(object sender, EventArgs e)
        {
            if (!checkBox_enabled.Checked)
            {
                if (!tNotifier.StopNotifier())
                    MessageBox.Show($"Failed to stop Telegram notificator", "Fail!", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                tNotifier.botToken = textBox_botToken.Text;
                try
                {
                    tNotifier.chatId = Convert.ToInt64(textBox_chatId.Text);
                }
                catch
                {
                    tNotifier.chatId = 0;
                    textBox_chatId.Text = "0";
                }
                foreach (var proxy in tNotifier.proxies)
                    proxy.isCurrent = false;
                var currentProxy = (TelegramProxy)comboBox_proxies.Items[comboBox_proxies.SelectedIndex];
                currentProxy.isCurrent = true;
                tNotifier.sound = textBox_sound.Text;
            }
            else
            {
                if (String.IsNullOrEmpty(textBox_chatId.Text))
                {
                    MessageBox.Show($"Invalid Bot Token", "Fail!", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                    return;
                }
                try
                {
                    tNotifier.chatId = Convert.ToInt64(textBox_chatId.Text);
                }
                catch
                {
                    MessageBox.Show($"Invalid chat Id", "Fail!", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                    return;
                }
                tNotifier.botToken = textBox_botToken.Text;
                foreach (var proxy in tNotifier.proxies)
                    proxy.isCurrent = false;
                var currentProxy = (TelegramProxy)comboBox_proxies.Items[comboBox_proxies.SelectedIndex];
                currentProxy.isCurrent = true;
                tNotifier.sound = textBox_sound.Text;
                button_apply.Text = "Connecting...";
                var startStatus = await tNotifier.StartNotifier();
                if (!startStatus)
                {
                    MessageBox.Show($"Failed to start Telegram notificator", "Fail!", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly);
                    button_apply.Text = "Ok";
                    checkBox_enabled.Checked = false;
                    return;
                }
            }
            Logger.AddLog($"Telegram notificator edited with new parameters: Token: {tNotifier.botToken}, chatId: {tNotifier.chatId}, proxy: {(from proxy in tNotifier.proxies where proxy.isCurrent select proxy).ToList().First().name}, error sound: {tNotifier.sound}");
            foreach (var proxy in tNotifier.proxies)
            {
                Logger.AddLog($"Telegram proxy: {proxy.name}");
            }
            Close();
        }

        private void Browse_sound_but_Click(object sender, EventArgs e)
        {
            OpenFileDialog OFD = new OpenFileDialog();
            OFD.Multiselect = false;
            OFD.Filter = "(*.mp3)|*.mp3|(*.wav)|*.wav";
            if (OFD.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    textBox_sound.Text = OFD.FileName;
                }
                catch { }
            }
        }

        private void Play_but_Click(object sender, EventArgs e)
        {
            if (System.IO.File.Exists(textBox_sound.Text))
                wmp.URL = textBox_sound.Text;
        }

        private void Stop_but_Click(object sender, EventArgs e)
        {
            wmp.controls.stop();
        }

        private void Clear_but_Click(object sender, EventArgs e)
        {
            textBox_sound.Text = "";
        }

        private void EditTNotificatorForm_Load(object sender, EventArgs e)
        {
            wmp.controls.stop();
            botClient = null;
            button_test.Text = "Test";
        }
    }
}
