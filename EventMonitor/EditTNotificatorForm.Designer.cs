﻿namespace EventMonitor
{
    partial class EditTNotificatorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditTNotificatorForm));
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_botToken = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_chatId = new System.Windows.Forms.TextBox();
            this.button_test = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBox_proxies = new System.Windows.Forms.ComboBox();
            this.button_proxy = new System.Windows.Forms.Button();
            this.button_apply = new System.Windows.Forms.Button();
            this.Clear_but = new System.Windows.Forms.Button();
            this.Stop_but = new System.Windows.Forms.Button();
            this.Play_but = new System.Windows.Forms.Button();
            this.Browse_sound_but = new System.Windows.Forms.Button();
            this.textBox_sound = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBox_enabled = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(63, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Bot Token";
            // 
            // textBox_botToken
            // 
            this.textBox_botToken.Location = new System.Drawing.Point(126, 24);
            this.textBox_botToken.Name = "textBox_botToken";
            this.textBox_botToken.Size = new System.Drawing.Size(210, 20);
            this.textBox_botToken.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(63, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Chat Id";
            // 
            // textBox_chatId
            // 
            this.textBox_chatId.Location = new System.Drawing.Point(126, 53);
            this.textBox_chatId.Name = "textBox_chatId";
            this.textBox_chatId.Size = new System.Drawing.Size(210, 20);
            this.textBox_chatId.TabIndex = 5;
            // 
            // button_test
            // 
            this.button_test.Location = new System.Drawing.Point(351, 53);
            this.button_test.Name = "button_test";
            this.button_test.Size = new System.Drawing.Size(75, 23);
            this.button_test.TabIndex = 6;
            this.button_test.Text = "Test";
            this.button_test.UseVisualStyleBackColor = true;
            this.button_test.Click += new System.EventHandler(this.Button_test_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(63, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(33, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Proxy";
            // 
            // comboBox_proxies
            // 
            this.comboBox_proxies.FormattingEnabled = true;
            this.comboBox_proxies.Location = new System.Drawing.Point(126, 84);
            this.comboBox_proxies.Name = "comboBox_proxies";
            this.comboBox_proxies.Size = new System.Drawing.Size(210, 21);
            this.comboBox_proxies.TabIndex = 9;
            // 
            // button_proxy
            // 
            this.button_proxy.Location = new System.Drawing.Point(351, 84);
            this.button_proxy.Name = "button_proxy";
            this.button_proxy.Size = new System.Drawing.Size(75, 23);
            this.button_proxy.TabIndex = 10;
            this.button_proxy.Text = "Proxy config";
            this.button_proxy.UseVisualStyleBackColor = true;
            this.button_proxy.Click += new System.EventHandler(this.Button_proxy_Click);
            // 
            // button_apply
            // 
            this.button_apply.Location = new System.Drawing.Point(204, 206);
            this.button_apply.Name = "button_apply";
            this.button_apply.Size = new System.Drawing.Size(75, 23);
            this.button_apply.TabIndex = 11;
            this.button_apply.Text = "Ok";
            this.button_apply.UseVisualStyleBackColor = true;
            this.button_apply.Click += new System.EventHandler(this.Button_apply_Click);
            // 
            // Clear_but
            // 
            this.Clear_but.Location = new System.Drawing.Point(343, 142);
            this.Clear_but.Name = "Clear_but";
            this.Clear_but.Size = new System.Drawing.Size(40, 23);
            this.Clear_but.TabIndex = 29;
            this.Clear_but.Text = "Clear";
            this.Clear_but.UseVisualStyleBackColor = true;
            this.Clear_but.Click += new System.EventHandler(this.Clear_but_Click);
            // 
            // Stop_but
            // 
            this.Stop_but.Location = new System.Drawing.Point(299, 162);
            this.Stop_but.Name = "Stop_but";
            this.Stop_but.Size = new System.Drawing.Size(37, 23);
            this.Stop_but.TabIndex = 28;
            this.Stop_but.Text = "Stop";
            this.Stop_but.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Stop_but.UseVisualStyleBackColor = true;
            this.Stop_but.Click += new System.EventHandler(this.Stop_but_Click);
            // 
            // Play_but
            // 
            this.Play_but.Location = new System.Drawing.Point(299, 119);
            this.Play_but.Name = "Play_but";
            this.Play_but.Size = new System.Drawing.Size(37, 23);
            this.Play_but.TabIndex = 27;
            this.Play_but.Text = "Play";
            this.Play_but.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Play_but.UseVisualStyleBackColor = true;
            this.Play_but.Click += new System.EventHandler(this.Play_but_Click);
            // 
            // Browse_sound_but
            // 
            this.Browse_sound_but.Location = new System.Drawing.Point(398, 142);
            this.Browse_sound_but.Name = "Browse_sound_but";
            this.Browse_sound_but.Size = new System.Drawing.Size(70, 23);
            this.Browse_sound_but.TabIndex = 26;
            this.Browse_sound_but.Text = "Browse...";
            this.Browse_sound_but.UseVisualStyleBackColor = true;
            this.Browse_sound_but.Click += new System.EventHandler(this.Browse_sound_but_Click);
            // 
            // textBox_sound
            // 
            this.textBox_sound.Location = new System.Drawing.Point(26, 142);
            this.textBox_sound.Name = "textBox_sound";
            this.textBox_sound.ReadOnly = true;
            this.textBox_sound.Size = new System.Drawing.Size(267, 20);
            this.textBox_sound.TabIndex = 25;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 126);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 24;
            this.label1.Text = "Error Sound";
            // 
            // checkBox_enabled
            // 
            this.checkBox_enabled.AutoSize = true;
            this.checkBox_enabled.Checked = true;
            this.checkBox_enabled.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_enabled.Location = new System.Drawing.Point(351, 25);
            this.checkBox_enabled.Name = "checkBox_enabled";
            this.checkBox_enabled.Size = new System.Drawing.Size(59, 17);
            this.checkBox_enabled.TabIndex = 30;
            this.checkBox_enabled.Text = "Enable";
            this.checkBox_enabled.UseVisualStyleBackColor = true;
            // 
            // EditTNotificatorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 241);
            this.Controls.Add(this.checkBox_enabled);
            this.Controls.Add(this.Clear_but);
            this.Controls.Add(this.Stop_but);
            this.Controls.Add(this.Play_but);
            this.Controls.Add(this.Browse_sound_but);
            this.Controls.Add(this.textBox_sound);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button_apply);
            this.Controls.Add(this.button_proxy);
            this.Controls.Add(this.comboBox_proxies);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button_test);
            this.Controls.Add(this.textBox_chatId);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox_botToken);
            this.Controls.Add(this.label2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EditTNotificatorForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Telegram Notification Settings";
            this.Load += new System.EventHandler(this.EditTNotificatorForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_botToken;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox_chatId;
        private System.Windows.Forms.Button button_test;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBox_proxies;
        private System.Windows.Forms.Button button_proxy;
        private System.Windows.Forms.Button button_apply;
        private System.Windows.Forms.Button Clear_but;
        private System.Windows.Forms.Button Stop_but;
        private System.Windows.Forms.Button Play_but;
        private System.Windows.Forms.Button Browse_sound_but;
        private System.Windows.Forms.TextBox textBox_sound;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkBox_enabled;
    }
}