﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WMPLib;

namespace EventMonitor
{
    //класс формы редактирования монитора подключений
    public partial class EditConnMonForm : Form
    {
        ConnMonitor connMonitor;
        private WindowsMediaPlayer wmp = new WindowsMediaPlayer();
        public EditConnMonForm(ConnMonitor connMonitor)
        {
            InitializeComponent();
            this.connMonitor = connMonitor;
            numericUpDown_delay.Value = this.connMonitor.Delay;
            textBox_sound.Text = this.connMonitor.Sound;
        }

        private void Browse_sound_but_Click(object sender, EventArgs e)
        {
            OpenFileDialog OFD = new OpenFileDialog();
            OFD.Multiselect = false;
            OFD.Filter = "(*.mp3)|*.mp3|(*.wav)|*.wav";
            if (OFD.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    textBox_sound.Text = OFD.FileName;
                }
                catch{ }
            }
        }

        private void Play_but_Click(object sender, EventArgs e)
        {
            if(File.Exists(textBox_sound.Text))
                wmp.URL = textBox_sound.Text;
        }

        private void Stop_but_Click(object sender, EventArgs e)
        {
            wmp.controls.stop();
        }

        private void Apply_but_Click(object sender, EventArgs e)
        {
            connMonitor.Sound = textBox_sound.Text;
            connMonitor.Delay = Convert.ToInt32(numericUpDown_delay.Value);

            Logger.AddLog($"{connMonitor.ToString()} edited with new parameters: delay: {connMonitor.Delay}, error sound: {connMonitor.Sound}");

            Close();
        }

        private void Clear_but_Click(object sender, EventArgs e)
        {
            textBox_sound.Text = "";
        }

        private void EditConnMonForm_Load(object sender, EventArgs e)
        {
            wmp.controls.stop();
        }
    }
}
