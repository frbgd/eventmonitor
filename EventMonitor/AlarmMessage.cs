﻿namespace EventMonitor
{
    //класс оповещения, создаётся во время триггеринга монитора, передаётся в функции оповещения
    public class AlarmMessage
    {
        public string Sound { get; }
        public string Title { get; }
        public string SubTitle { get; }
        public string Body { get; }

        public AlarmMessage(string sound = "", string title = "Null", string subtitle = "", string body = "")
        {
            Sound = sound;
            Title = title;
            SubTitle = subtitle;
            Body = body;
        }
    }
}
