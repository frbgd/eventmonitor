﻿using Newtonsoft.Json;
using System;

namespace EventMonitor
{

    //абстрактный класс монитора, подразумевается, что в будущем программа будет работать не только с Outlook
    public abstract class Monitor
    {
        [JsonIgnore]
        public int Id { get; set; }
        public string Name { get; set; } = "";
        public string Path { get; set; } = "";
        public string Sound { get; set; } = "";
        public bool Status { get; set; } = true;
        public bool tNotification { get; set; } = true;
        public bool sNotification { get; set; } = true;

        public IConnection Connection;
        [JsonIgnore]
        public EventHandler<AlarmMessage> EventMessage { get; set; }

        public bool Establish_Connection()
        {
            return Connection.Check(Path);
        }

        public abstract void Start();

        public abstract void Stop();
    }
}
