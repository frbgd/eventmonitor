﻿namespace EventMonitor
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.CurrentMonitor = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.AllMonitor = new System.Windows.Forms.ToolStripStatusLabel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.logPanel = new System.Windows.Forms.TableLayoutPanel();
            this.listView_messages = new System.Windows.Forms.ListView();
            this.cjn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.qweqw = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.Sound_off_butt = new System.Windows.Forms.Button();
            this.Clear_logs_butt = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.dataGrid_Connectors = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StatusConnector = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Sound = new System.Windows.Forms.DataGridViewImageColumn();
            this.Tlgrm = new System.Windows.Forms.DataGridViewImageColumn();
            this.Source = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Play = new System.Windows.Forms.DataGridViewImageColumn();
            this.Delete = new System.Windows.Forms.DataGridViewImageColumn();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.richTextBox_Path = new System.Windows.Forms.RichTextBox();
            this.open_sound_folder_but = new System.Windows.Forms.Button();
            this.richTextBox_Sound = new System.Windows.Forms.RichTextBox();
            this.richTextBox_Name = new System.Windows.Forms.RichTextBox();
            this.Start_Stop_but = new System.Windows.Forms.Button();
            this.Edit_but = new System.Windows.Forms.Button();
            this.Delete_but = new System.Windows.Forms.Button();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.New_but = new System.Windows.Forms.Button();
            this.Open_but = new System.Windows.Forms.Button();
            this.Save_but = new System.Windows.Forms.Button();
            this.Save_As_but = new System.Windows.Forms.Button();
            this.file_path = new System.Windows.Forms.Label();
            this.pictureBox_isSaved = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.button_new_connector = new System.Windows.Forms.Button();
            this.button_start_all = new System.Windows.Forms.Button();
            this.button_stop_all = new System.Windows.Forms.Button();
            this.button_delete_all = new System.Windows.Forms.Button();
            this.button_rotate_status = new System.Windows.Forms.Button();
            this.button_conn_mon_config = new System.Windows.Forms.Button();
            this.button_tnotification = new System.Windows.Forms.Button();
            this.statusStrip1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.logPanel.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid_Connectors)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_isSaved)).BeginInit();
            this.tableLayoutPanel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.CurrentMonitor,
            this.toolStripStatusLabel2,
            this.AllMonitor});
            this.statusStrip1.Location = new System.Drawing.Point(0, 639);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1184, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(55, 17);
            this.toolStripStatusLabel1.Text = "Running:";
            // 
            // CurrentMonitor
            // 
            this.CurrentMonitor.Name = "CurrentMonitor";
            this.CurrentMonitor.Size = new System.Drawing.Size(13, 17);
            this.CurrentMonitor.Text = "0";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(12, 17);
            this.toolStripStatusLabel2.Text = "/";
            // 
            // AllMonitor
            // 
            this.AllMonitor.Name = "AllMonitor";
            this.AllMonitor.Size = new System.Drawing.Size(13, 17);
            this.AllMonitor.Text = "0";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 400F));
            this.tableLayoutPanel1.Controls.Add(this.logPanel, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1184, 639);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // logPanel
            // 
            this.logPanel.ColumnCount = 1;
            this.logPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.logPanel.Controls.Add(this.listView_messages, 0, 0);
            this.logPanel.Controls.Add(this.tableLayoutPanel5, 0, 1);
            this.logPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.logPanel.Location = new System.Drawing.Point(787, 3);
            this.logPanel.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.logPanel.Name = "logPanel";
            this.logPanel.RowCount = 2;
            this.logPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.logPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.logPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.logPanel.Size = new System.Drawing.Size(394, 636);
            this.logPanel.TabIndex = 0;
            // 
            // listView_messages
            // 
            this.listView_messages.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listView_messages.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.cjn,
            this.qweqw});
            this.listView_messages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView_messages.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.listView_messages.HideSelection = false;
            this.listView_messages.Location = new System.Drawing.Point(3, 3);
            this.listView_messages.Name = "listView_messages";
            this.listView_messages.Size = new System.Drawing.Size(388, 550);
            this.listView_messages.TabIndex = 5;
            this.listView_messages.TileSize = new System.Drawing.Size(380, 50);
            this.listView_messages.UseCompatibleStateImageBehavior = false;
            this.listView_messages.View = System.Windows.Forms.View.Tile;
            // 
            // cjn
            // 
            this.cjn.Width = 75;
            // 
            // qweqw
            // 
            this.qweqw.Text = "text";
            this.qweqw.Width = 160;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel5.Controls.Add(this.Sound_off_butt, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.Clear_logs_butt, 1, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 559);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(388, 74);
            this.tableLayoutPanel5.TabIndex = 6;
            // 
            // Sound_off_butt
            // 
            this.Sound_off_butt.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Sound_off_butt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Sound_off_butt.Image = ((System.Drawing.Image)(resources.GetObject("Sound_off_butt.Image")));
            this.Sound_off_butt.Location = new System.Drawing.Point(3, 0);
            this.Sound_off_butt.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.Sound_off_butt.Name = "Sound_off_butt";
            this.Sound_off_butt.Size = new System.Drawing.Size(302, 74);
            this.Sound_off_butt.TabIndex = 0;
            this.Sound_off_butt.UseVisualStyleBackColor = true;
            this.Sound_off_butt.Click += new System.EventHandler(this.Sound_off_butt_Click);
            // 
            // Clear_logs_butt
            // 
            this.Clear_logs_butt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Clear_logs_butt.Image = ((System.Drawing.Image)(resources.GetObject("Clear_logs_butt.Image")));
            this.Clear_logs_butt.Location = new System.Drawing.Point(311, 0);
            this.Clear_logs_butt.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.Clear_logs_butt.Name = "Clear_logs_butt";
            this.Clear_logs_butt.Size = new System.Drawing.Size(74, 74);
            this.Clear_logs_butt.TabIndex = 1;
            this.Clear_logs_butt.UseVisualStyleBackColor = true;
            this.Clear_logs_butt.Click += new System.EventHandler(this.Clear_logs_butt_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70F));
            this.tableLayoutPanel2.Controls.Add(this.dataGrid_Connectors, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel4, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel6, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(3, 3, 0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 51F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 51F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(781, 636);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // dataGrid_Connectors
            // 
            this.dataGrid_Connectors.AllowUserToAddRows = false;
            this.dataGrid_Connectors.AllowUserToDeleteRows = false;
            this.dataGrid_Connectors.AllowUserToResizeColumns = false;
            this.dataGrid_Connectors.AllowUserToResizeRows = false;
            this.dataGrid_Connectors.BackgroundColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGrid_Connectors.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGrid_Connectors.ColumnHeadersHeight = 25;
            this.dataGrid_Connectors.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.StatusConnector,
            this.Sound,
            this.Tlgrm,
            this.Source,
            this.Play,
            this.Delete});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGrid_Connectors.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGrid_Connectors.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGrid_Connectors.Location = new System.Drawing.Point(3, 105);
            this.dataGrid_Connectors.Name = "dataGrid_Connectors";
            this.dataGrid_Connectors.ReadOnly = true;
            this.dataGrid_Connectors.RowHeadersVisible = false;
            this.dataGrid_Connectors.RowTemplate.Height = 35;
            this.dataGrid_Connectors.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGrid_Connectors.Size = new System.Drawing.Size(775, 478);
            this.dataGrid_Connectors.TabIndex = 1;
            this.dataGrid_Connectors.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGrid_Connectors_CellClick);
            this.dataGrid_Connectors.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGrid_Connectors_CellDoubleClick);
            // 
            // id
            // 
            this.id.Frozen = true;
            this.id.HeaderText = "id";
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Visible = false;
            // 
            // StatusConnector
            // 
            this.StatusConnector.Frozen = true;
            this.StatusConnector.HeaderText = "Status";
            this.StatusConnector.MinimumWidth = 50;
            this.StatusConnector.Name = "StatusConnector";
            this.StatusConnector.ReadOnly = true;
            this.StatusConnector.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.StatusConnector.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.StatusConnector.Width = 50;
            // 
            // Sound
            // 
            this.Sound.Frozen = true;
            this.Sound.HeaderText = "Sound";
            this.Sound.MinimumWidth = 50;
            this.Sound.Name = "Sound";
            this.Sound.ReadOnly = true;
            this.Sound.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Sound.Width = 50;
            // 
            // Tlgrm
            // 
            this.Tlgrm.Frozen = true;
            this.Tlgrm.HeaderText = "Telegram";
            this.Tlgrm.MinimumWidth = 50;
            this.Tlgrm.Name = "Tlgrm";
            this.Tlgrm.ReadOnly = true;
            this.Tlgrm.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Tlgrm.Width = 50;
            // 
            // Source
            // 
            this.Source.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Source.HeaderText = "Name (double-click for edit)";
            this.Source.MinimumWidth = 100;
            this.Source.Name = "Source";
            this.Source.ReadOnly = true;
            this.Source.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Play
            // 
            this.Play.HeaderText = "Play";
            this.Play.Image = ((System.Drawing.Image)(resources.GetObject("Play.Image")));
            this.Play.MinimumWidth = 50;
            this.Play.Name = "Play";
            this.Play.ReadOnly = true;
            this.Play.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Play.Width = 50;
            // 
            // Delete
            // 
            this.Delete.HeaderText = "Delete";
            this.Delete.Image = ((System.Drawing.Image)(resources.GetObject("Delete.Image")));
            this.Delete.MinimumWidth = 50;
            this.Delete.Name = "Delete";
            this.Delete.ReadOnly = true;
            this.Delete.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Delete.Width = 50;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 7;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            this.tableLayoutPanel3.Controls.Add(this.richTextBox_Path, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.open_sound_folder_but, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.richTextBox_Sound, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.richTextBox_Name, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.Start_Stop_but, 4, 0);
            this.tableLayoutPanel3.Controls.Add(this.Edit_but, 5, 0);
            this.tableLayoutPanel3.Controls.Add(this.Delete_but, 6, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 586);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(781, 50);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // richTextBox_Path
            // 
            this.richTextBox_Path.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox_Path.ForeColor = System.Drawing.SystemColors.WindowText;
            this.richTextBox_Path.Location = new System.Drawing.Point(201, 3);
            this.richTextBox_Path.Name = "richTextBox_Path";
            this.richTextBox_Path.ReadOnly = true;
            this.richTextBox_Path.Size = new System.Drawing.Size(192, 44);
            this.richTextBox_Path.TabIndex = 2;
            this.richTextBox_Path.Text = "";
            // 
            // open_sound_folder_but
            // 
            this.open_sound_folder_but.Dock = System.Windows.Forms.DockStyle.Fill;
            this.open_sound_folder_but.Image = ((System.Drawing.Image)(resources.GetObject("open_sound_folder_but.Image")));
            this.open_sound_folder_but.Location = new System.Drawing.Point(603, 3);
            this.open_sound_folder_but.Name = "open_sound_folder_but";
            this.open_sound_folder_but.Size = new System.Drawing.Size(39, 44);
            this.open_sound_folder_but.TabIndex = 8;
            this.open_sound_folder_but.UseVisualStyleBackColor = true;
            this.open_sound_folder_but.Click += new System.EventHandler(this.Open_sound_folder_but_Click);
            // 
            // richTextBox_Sound
            // 
            this.richTextBox_Sound.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox_Sound.Location = new System.Drawing.Point(399, 3);
            this.richTextBox_Sound.Name = "richTextBox_Sound";
            this.richTextBox_Sound.ReadOnly = true;
            this.richTextBox_Sound.Size = new System.Drawing.Size(198, 44);
            this.richTextBox_Sound.TabIndex = 9;
            this.richTextBox_Sound.Text = "";
            // 
            // richTextBox_Name
            // 
            this.richTextBox_Name.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox_Name.Location = new System.Drawing.Point(3, 3);
            this.richTextBox_Name.Name = "richTextBox_Name";
            this.richTextBox_Name.ReadOnly = true;
            this.richTextBox_Name.Size = new System.Drawing.Size(192, 44);
            this.richTextBox_Name.TabIndex = 10;
            this.richTextBox_Name.Text = "";
            // 
            // Start_Stop_but
            // 
            this.Start_Stop_but.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Start_Stop_but.Image = ((System.Drawing.Image)(resources.GetObject("Start_Stop_but.Image")));
            this.Start_Stop_but.Location = new System.Drawing.Point(648, 3);
            this.Start_Stop_but.Name = "Start_Stop_but";
            this.Start_Stop_but.Size = new System.Drawing.Size(39, 44);
            this.Start_Stop_but.TabIndex = 11;
            this.Start_Stop_but.UseVisualStyleBackColor = true;
            this.Start_Stop_but.Click += new System.EventHandler(this.Start_Stop_but_Click);
            // 
            // Edit_but
            // 
            this.Edit_but.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Edit_but.Image = ((System.Drawing.Image)(resources.GetObject("Edit_but.Image")));
            this.Edit_but.Location = new System.Drawing.Point(693, 3);
            this.Edit_but.Name = "Edit_but";
            this.Edit_but.Size = new System.Drawing.Size(39, 44);
            this.Edit_but.TabIndex = 12;
            this.Edit_but.UseVisualStyleBackColor = true;
            this.Edit_but.Click += new System.EventHandler(this.Edit_but_Click);
            // 
            // Delete_but
            // 
            this.Delete_but.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Delete_but.Image = ((System.Drawing.Image)(resources.GetObject("Delete_but.Image")));
            this.Delete_but.Location = new System.Drawing.Point(738, 3);
            this.Delete_but.Name = "Delete_but";
            this.Delete_but.Size = new System.Drawing.Size(40, 44);
            this.Delete_but.TabIndex = 13;
            this.Delete_but.UseVisualStyleBackColor = true;
            this.Delete_but.Click += new System.EventHandler(this.Delete_but_Click);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 6;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel4.Controls.Add(this.New_but, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.Open_but, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.Save_but, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.Save_As_but, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.file_path, 4, 0);
            this.tableLayoutPanel4.Controls.Add(this.pictureBox_isSaved, 5, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(775, 45);
            this.tableLayoutPanel4.TabIndex = 2;
            // 
            // New_but
            // 
            this.New_but.Dock = System.Windows.Forms.DockStyle.Fill;
            this.New_but.Image = ((System.Drawing.Image)(resources.GetObject("New_but.Image")));
            this.New_but.Location = new System.Drawing.Point(3, 3);
            this.New_but.Name = "New_but";
            this.New_but.Size = new System.Drawing.Size(39, 39);
            this.New_but.TabIndex = 0;
            this.New_but.UseVisualStyleBackColor = true;
            this.New_but.Click += new System.EventHandler(this.New_but_Click);
            // 
            // Open_but
            // 
            this.Open_but.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Open_but.Image = ((System.Drawing.Image)(resources.GetObject("Open_but.Image")));
            this.Open_but.Location = new System.Drawing.Point(48, 3);
            this.Open_but.Name = "Open_but";
            this.Open_but.Size = new System.Drawing.Size(39, 39);
            this.Open_but.TabIndex = 1;
            this.Open_but.UseVisualStyleBackColor = true;
            this.Open_but.Click += new System.EventHandler(this.Open_but_Click);
            // 
            // Save_but
            // 
            this.Save_but.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Save_but.Image = ((System.Drawing.Image)(resources.GetObject("Save_but.Image")));
            this.Save_but.Location = new System.Drawing.Point(93, 3);
            this.Save_but.Name = "Save_but";
            this.Save_but.Size = new System.Drawing.Size(39, 39);
            this.Save_but.TabIndex = 2;
            this.Save_but.UseVisualStyleBackColor = true;
            this.Save_but.Click += new System.EventHandler(this.Save_but_Click);
            // 
            // Save_As_but
            // 
            this.Save_As_but.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Save_As_but.Image = ((System.Drawing.Image)(resources.GetObject("Save_As_but.Image")));
            this.Save_As_but.Location = new System.Drawing.Point(138, 3);
            this.Save_As_but.Name = "Save_As_but";
            this.Save_As_but.Size = new System.Drawing.Size(39, 39);
            this.Save_As_but.TabIndex = 3;
            this.Save_As_but.UseVisualStyleBackColor = true;
            this.Save_As_but.Click += new System.EventHandler(this.Save_As_but_Click);
            // 
            // file_path
            // 
            this.file_path.AutoSize = true;
            this.file_path.Dock = System.Windows.Forms.DockStyle.Fill;
            this.file_path.Location = new System.Drawing.Point(183, 0);
            this.file_path.Name = "file_path";
            this.file_path.Size = new System.Drawing.Size(544, 45);
            this.file_path.TabIndex = 4;
            this.file_path.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox_isSaved
            // 
            this.pictureBox_isSaved.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox_isSaved.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox_isSaved.Image")));
            this.pictureBox_isSaved.Location = new System.Drawing.Point(733, 3);
            this.pictureBox_isSaved.Name = "pictureBox_isSaved";
            this.pictureBox_isSaved.Size = new System.Drawing.Size(39, 39);
            this.pictureBox_isSaved.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox_isSaved.TabIndex = 5;
            this.pictureBox_isSaved.TabStop = false;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 8;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel6.Controls.Add(this.button_new_connector, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.button_start_all, 4, 0);
            this.tableLayoutPanel6.Controls.Add(this.button_stop_all, 5, 0);
            this.tableLayoutPanel6.Controls.Add(this.button_delete_all, 7, 0);
            this.tableLayoutPanel6.Controls.Add(this.button_rotate_status, 6, 0);
            this.tableLayoutPanel6.Controls.Add(this.button_conn_mon_config, 3, 0);
            this.tableLayoutPanel6.Controls.Add(this.button_tnotification, 2, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 54);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(775, 45);
            this.tableLayoutPanel6.TabIndex = 3;
            // 
            // button_new_connector
            // 
            this.button_new_connector.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_new_connector.Image = ((System.Drawing.Image)(resources.GetObject("button_new_connector.Image")));
            this.button_new_connector.Location = new System.Drawing.Point(3, 3);
            this.button_new_connector.Name = "button_new_connector";
            this.button_new_connector.Size = new System.Drawing.Size(39, 39);
            this.button_new_connector.TabIndex = 0;
            this.button_new_connector.UseVisualStyleBackColor = true;
            this.button_new_connector.Click += new System.EventHandler(this.Button_new_connector_Click);
            // 
            // button_start_all
            // 
            this.button_start_all.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_start_all.Image = ((System.Drawing.Image)(resources.GetObject("button_start_all.Image")));
            this.button_start_all.Location = new System.Drawing.Point(598, 3);
            this.button_start_all.Name = "button_start_all";
            this.button_start_all.Size = new System.Drawing.Size(39, 39);
            this.button_start_all.TabIndex = 1;
            this.button_start_all.UseVisualStyleBackColor = true;
            this.button_start_all.Click += new System.EventHandler(this.Button_start_all_Click);
            // 
            // button_stop_all
            // 
            this.button_stop_all.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_stop_all.Image = ((System.Drawing.Image)(resources.GetObject("button_stop_all.Image")));
            this.button_stop_all.Location = new System.Drawing.Point(643, 3);
            this.button_stop_all.Name = "button_stop_all";
            this.button_stop_all.Size = new System.Drawing.Size(39, 39);
            this.button_stop_all.TabIndex = 2;
            this.button_stop_all.UseVisualStyleBackColor = true;
            this.button_stop_all.Click += new System.EventHandler(this.Button_stop_all_Click);
            // 
            // button_delete_all
            // 
            this.button_delete_all.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_delete_all.Image = ((System.Drawing.Image)(resources.GetObject("button_delete_all.Image")));
            this.button_delete_all.Location = new System.Drawing.Point(733, 3);
            this.button_delete_all.Name = "button_delete_all";
            this.button_delete_all.Size = new System.Drawing.Size(39, 39);
            this.button_delete_all.TabIndex = 3;
            this.button_delete_all.UseVisualStyleBackColor = true;
            this.button_delete_all.Click += new System.EventHandler(this.Button_delete_all_Click);
            // 
            // button_rotate_status
            // 
            this.button_rotate_status.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_rotate_status.Image = ((System.Drawing.Image)(resources.GetObject("button_rotate_status.Image")));
            this.button_rotate_status.Location = new System.Drawing.Point(688, 3);
            this.button_rotate_status.Name = "button_rotate_status";
            this.button_rotate_status.Size = new System.Drawing.Size(39, 39);
            this.button_rotate_status.TabIndex = 4;
            this.button_rotate_status.UseVisualStyleBackColor = true;
            this.button_rotate_status.Click += new System.EventHandler(this.Button_rotate_status_Click);
            // 
            // button_conn_mon_config
            // 
            this.button_conn_mon_config.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_conn_mon_config.Image = ((System.Drawing.Image)(resources.GetObject("button_conn_mon_config.Image")));
            this.button_conn_mon_config.Location = new System.Drawing.Point(553, 3);
            this.button_conn_mon_config.Name = "button_conn_mon_config";
            this.button_conn_mon_config.Size = new System.Drawing.Size(39, 39);
            this.button_conn_mon_config.TabIndex = 5;
            this.button_conn_mon_config.UseVisualStyleBackColor = true;
            this.button_conn_mon_config.Click += new System.EventHandler(this.Button_conn_mon_config_Click);
            // 
            // button_tnotification
            // 
            this.button_tnotification.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_tnotification.Image = ((System.Drawing.Image)(resources.GetObject("button_tnotification.Image")));
            this.button_tnotification.Location = new System.Drawing.Point(508, 3);
            this.button_tnotification.Name = "button_tnotification";
            this.button_tnotification.Size = new System.Drawing.Size(39, 39);
            this.button_tnotification.TabIndex = 6;
            this.button_tnotification.UseVisualStyleBackColor = true;
            this.button_tnotification.Click += new System.EventHandler(this.Button_tnotification_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1184, 661);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(800, 400);
            this.Name = "MainForm";
            this.Text = "Event Monitor";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.logPanel.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid_Connectors)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_isSaved)).EndInit();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel logPanel;
        private System.Windows.Forms.ListView listView_messages;
        private System.Windows.Forms.ColumnHeader cjn;
        private System.Windows.Forms.ColumnHeader qweqw;
        private System.Windows.Forms.Button Sound_off_butt;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel CurrentMonitor;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel AllMonitor;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.DataGridView dataGrid_Connectors;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.RichTextBox richTextBox_Path;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Button New_but;
        private System.Windows.Forms.Button Open_but;
        private System.Windows.Forms.Button Save_but;
        private System.Windows.Forms.Button Save_As_but;
        private System.Windows.Forms.Label file_path;
        private System.Windows.Forms.Button open_sound_folder_but;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Button Clear_logs_butt;
        private System.Windows.Forms.PictureBox pictureBox_isSaved;
        private System.Windows.Forms.RichTextBox richTextBox_Sound;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Button button_new_connector;
        private System.Windows.Forms.Button button_start_all;
        private System.Windows.Forms.Button button_stop_all;
        private System.Windows.Forms.Button button_delete_all;
        private System.Windows.Forms.Button button_rotate_status;
        private System.Windows.Forms.RichTextBox richTextBox_Name;
        private System.Windows.Forms.Button Start_Stop_but;
        private System.Windows.Forms.Button Edit_but;
        private System.Windows.Forms.Button Delete_but;
        private System.Windows.Forms.Button button_conn_mon_config;
        private System.Windows.Forms.Button button_tnotification;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn StatusConnector;
        private System.Windows.Forms.DataGridViewImageColumn Sound;
        private System.Windows.Forms.DataGridViewImageColumn Tlgrm;
        private System.Windows.Forms.DataGridViewTextBoxColumn Source;
        private System.Windows.Forms.DataGridViewImageColumn Play;
        private System.Windows.Forms.DataGridViewImageColumn Delete;
    }
}