﻿namespace EventMonitor
{
    //класс-xor-шифрователь строк
    public static class XORCipher
    {
        static string pass = "M2:pf+";
        //генератор повторений пароля
        private static string GetRepeatKey(string s, int n)
        {
            var r = s;
            while (r.Length < n)
            {
                r += r;
            }

            return r.Substring(0, n);
        }

        //метод шифрования/дешифровки
        private static string Cipher(string text)
        {
            var currentKey = GetRepeatKey(pass, text.Length);
            var res = string.Empty;
            for (var i = 0; i < text.Length; i++)
            {
                res += ((char)(text[i] ^ currentKey[i])).ToString();
            }

            return res;
        }

        //шифрование текста
        public static string Encrypt(string plainText)
            => Cipher(plainText);

        //расшифровка текста
        public static string Decrypt(string encryptedText)
            => Cipher(encryptedText);
    }
}
