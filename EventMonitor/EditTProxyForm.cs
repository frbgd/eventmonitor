﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace EventMonitor
{
    //класс формы редактирования Телеграм прокси
    public partial class EditTProxyForm : Form
    {
        TelegramNotifier tNotifier;
        TelegramProxy selectedProxy;
        public EditTProxyForm(TelegramNotifier tNotifier)
        {
            this.tNotifier = tNotifier;
            InitializeComponent();
            foreach (var proxy in this.tNotifier.proxies)
            {
                if(String.IsNullOrEmpty(proxy.host))
                {
                    continue;
                }
                listBox_ProxiesList.Items.Add(proxy);
            }
        }

        private void ListBox_ProxiesList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox_ProxiesList.SelectedIndex > -1)
            {
                selectedProxy = (TelegramProxy)listBox_ProxiesList.Items[listBox_ProxiesList.SelectedIndex];
                textBox_host.Text = selectedProxy.host;
                if (selectedProxy.port != 0)
                    textBox_port.Text = selectedProxy.port.ToString();
                else
                    textBox_port.Text = "";
                textBox_login.Text = selectedProxy.login;
                textBox_pass.Text = selectedProxy.pass;
            }
        }

        private void Button_add_Click(object sender, EventArgs e)
        {
            var newProxy = new TelegramProxy(false);
            listBox_ProxiesList.Items.Add(newProxy);
            listBox_ProxiesList.SelectedIndex = listBox_ProxiesList.Items.Count - 1;
        }

        private void Button_del_Click(object sender, EventArgs e)
        {
            if (listBox_ProxiesList.Items.Count > 0)
            {
                listBox_ProxiesList.Items.RemoveAt(listBox_ProxiesList.SelectedIndex);
                textBox_host.Text = "";
                textBox_port.Text = "";
                textBox_login.Text = "";
                textBox_pass.Text = "";
            }
        }

        private void button_save_Click(object sender, EventArgs e)
        {
            if (listBox_ProxiesList.Items.Count > 0)
            {
                if (listBox_ProxiesList.SelectedIndex > -1)
                {
                    if (!String.IsNullOrEmpty(textBox_host.Text))
                    {
                        int port;
                        try
                        {
                            port = Convert.ToInt32(textBox_port.Text);
                        }
                        catch
                        {
                            MessageBox.Show("Invalid port value!");
                            return;
                        }
                        selectedProxy.SetProxy(textBox_host.Text, port, textBox_login.Text, textBox_pass.Text);
                    }
                    else
                    {
                        MessageBox.Show("Name field must be non-empty!");
                        return;
                    }
                }
                tNotifier.proxies = new List<TelegramProxy>();
                tNotifier.proxies.Add(new TelegramProxy(true));
                foreach (TelegramProxy proxy in listBox_ProxiesList.Items)
                {
                    tNotifier.proxies.Add(proxy);
                }
                listBox_ProxiesList.Items.Clear();
                foreach (var proxy in this.tNotifier.proxies)
                {
                    if (String.IsNullOrEmpty(proxy.host))
                    {
                        continue;
                    }
                    listBox_ProxiesList.Items.Add(proxy);
                }
            }
        }
    }
}
