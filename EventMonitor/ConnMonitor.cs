﻿using Newtonsoft.Json;

namespace EventMonitor
{
    //абстрактный класс монитора подключений - подразумевается, что в дальнейшем программа будет работать не только с Outlook
    public abstract class ConnMonitor
    {
        public IConnection connection;
        [JsonIgnore]
        public MainForm mainForm { get; set; }
        protected int delay = 10000;
        public string Sound = "";
        public ConnStatus status = ConnStatus.checking;

        public int Delay
        {
            get
            {
                return delay / 1000;
            }
            set
            {
                if (value < 3)
                    delay = 3000;
                else if (value > 60)
                    delay = 60000;
                else
                    delay = value * 1000;

            }
        }
        public enum ConnStatus
        {
            connected,
            checking,
            disconnected
        }

        public ConnMonitor(MainForm mainForm)
        {
            this.mainForm = mainForm;
        }

        public abstract void StartMonitoring();

        public abstract void StopMonitoring();

        protected abstract void AsyncAction();
    }
}
