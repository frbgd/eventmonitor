﻿using System.Threading;
using System.Threading.Tasks;

namespace EventMonitor.Outlook
{
    //класс монитора подключения Outlook
    public class OutlookConnMonitor : ConnMonitor
    {
        public OutlookConnMonitor(MainForm mainForm) : base(mainForm) { }

        public override void StartMonitoring()
        {
            connection = new OutlookConnection();
            AsyncAction();
            Logger.AddLog($"Outlook connection monitor started with parameters: delay: {delay}, error sound: {Sound}");
        }

        public override void StopMonitoring()
        {
            connection = null;
            Logger.AddLog("Outlook connection monitor stopped");
        }

        //асинхронная работа монитора
        protected override async void AsyncAction()
        {
            await Task.Run(() => {
                while (true)
                {
                    if (connection != null)
                    {
                        mainForm.changeConnPic(ConnStatus.checking, this);
                        Thread.Sleep(250);
                        if (connection.Check())
                        {
                            mainForm.changeConnPic(ConnStatus.connected, this);
                            status = ConnStatus.connected;
                        }
                        else
                        {
                            mainForm.changeConnPic(ConnStatus.disconnected, this);
                            status = ConnStatus.disconnected;
                        }
                    }
                    else
                        break;
                    Thread.Sleep(delay);
                }
            });
        }
    }
}
