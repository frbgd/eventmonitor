﻿using Microsoft.Office.Interop.Outlook;

namespace EventMonitor.Outlook
{
    //монитор Outlook
    public class OutlookMonitor : Monitor
    {
        private OutlookConnection connection;
        public OutlookMonitor()
        {
            Connection = new OutlookConnection();
            connection = Connection as OutlookConnection;
        }
        public override void Start()
        {
            try
            {
                connection.Box.ItemAdd += Box_ItemAdd;
                Logger.AddLog($"OutlookMonitor {Name} started");
                Status = true;
            }
            catch (System.Exception ex)
            {
                Status = false;
                Logger.AddLog($"ERROR: OutlookMonitor {Name} failed to start. {ex.Message}");
                throw new System.Exception("Need to restart");
            }
        }

        private void Box_ItemAdd(object Item)
        {
            MailItem mailItem = (MailItem)Item;
            string body = $"From: {mailItem.SenderName} ({mailItem.SenderEmailAddress})\nTo: {mailItem.To}\nCC: {mailItem.CC}\nSubject: {mailItem.Subject}\n\nBody:\n{mailItem.Body}";
            Logger.AddLog($"OutlookMonitor {Name}: New message in Outlook folder {Path}");
            AlarmMessage message = new AlarmMessage(Sound, $"Outlook: {Name}", $"Message in {Path.Remove(0, Path.LastIndexOf('\\') + 1)}", body);
            EventMessage(this, message);
        }

        public override void Stop()
        {
            try
            {
                connection.Box.ItemAdd -= Box_ItemAdd;
                Logger.AddLog($"OutlookMonitor {Name} stopped");
                Status = false;
            }

            catch (System.Exception ex)
            {
                Status = true;
                Logger.AddLog($"ERROR: OutlookMonitor {Name} failed to stop. {ex.Message}");
                throw new System.Exception("Need to restart");
            }
        }
    }
}
