﻿namespace EventMonitor.Outlook
{
    partial class EditOutlookConnectorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditOutlookConnectorForm));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_sound = new System.Windows.Forms.TextBox();
            this.textBox_folder = new System.Windows.Forms.TextBox();
            this.Browse_folder_but = new System.Windows.Forms.Button();
            this.Browse_sound_but = new System.Windows.Forms.Button();
            this.Apply_but = new System.Windows.Forms.Button();
            this.textBox_name = new System.Windows.Forms.TextBox();
            this.checkBox_enabled = new System.Windows.Forms.CheckBox();
            this.Play_but = new System.Windows.Forms.Button();
            this.Stop_but = new System.Windows.Forms.Button();
            this.Clear_sound_but = new System.Windows.Forms.Button();
            this.checkBox_telegram = new System.Windows.Forms.CheckBox();
            this.checkBox_Sound = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(10, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Folder";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Sound";
            // 
            // textBox_sound
            // 
            this.textBox_sound.Location = new System.Drawing.Point(55, 95);
            this.textBox_sound.Name = "textBox_sound";
            this.textBox_sound.ReadOnly = true;
            this.textBox_sound.Size = new System.Drawing.Size(240, 20);
            this.textBox_sound.TabIndex = 6;
            // 
            // textBox_folder
            // 
            this.textBox_folder.Location = new System.Drawing.Point(55, 55);
            this.textBox_folder.Name = "textBox_folder";
            this.textBox_folder.ReadOnly = true;
            this.textBox_folder.Size = new System.Drawing.Size(333, 20);
            this.textBox_folder.TabIndex = 3;
            // 
            // Browse_folder_but
            // 
            this.Browse_folder_but.Location = new System.Drawing.Point(400, 52);
            this.Browse_folder_but.Name = "Browse_folder_but";
            this.Browse_folder_but.Size = new System.Drawing.Size(70, 23);
            this.Browse_folder_but.TabIndex = 4;
            this.Browse_folder_but.Text = "Browse...";
            this.Browse_folder_but.UseVisualStyleBackColor = true;
            this.Browse_folder_but.Click += new System.EventHandler(this.Browse_folder_but_Click);
            // 
            // Browse_sound_but
            // 
            this.Browse_sound_but.Location = new System.Drawing.Point(400, 95);
            this.Browse_sound_but.Name = "Browse_sound_but";
            this.Browse_sound_but.Size = new System.Drawing.Size(70, 23);
            this.Browse_sound_but.TabIndex = 7;
            this.Browse_sound_but.Text = "Browse...";
            this.Browse_sound_but.UseVisualStyleBackColor = true;
            this.Browse_sound_but.Click += new System.EventHandler(this.Browse_sound_but_Click);
            // 
            // Apply_but
            // 
            this.Apply_but.Location = new System.Drawing.Point(210, 152);
            this.Apply_but.Name = "Apply_but";
            this.Apply_but.Size = new System.Drawing.Size(75, 23);
            this.Apply_but.TabIndex = 8;
            this.Apply_but.Text = "Ok";
            this.Apply_but.UseVisualStyleBackColor = true;
            this.Apply_but.Click += new System.EventHandler(this.Apply_but_Click);
            // 
            // textBox_name
            // 
            this.textBox_name.Location = new System.Drawing.Point(55, 15);
            this.textBox_name.Name = "textBox_name";
            this.textBox_name.Size = new System.Drawing.Size(333, 20);
            this.textBox_name.TabIndex = 1;
            // 
            // checkBox_enabled
            // 
            this.checkBox_enabled.AutoSize = true;
            this.checkBox_enabled.Checked = true;
            this.checkBox_enabled.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_enabled.Location = new System.Drawing.Point(400, 15);
            this.checkBox_enabled.Name = "checkBox_enabled";
            this.checkBox_enabled.Size = new System.Drawing.Size(59, 17);
            this.checkBox_enabled.TabIndex = 9;
            this.checkBox_enabled.Text = "Enable";
            this.checkBox_enabled.UseVisualStyleBackColor = true;
            // 
            // Play_but
            // 
            this.Play_but.Location = new System.Drawing.Point(307, 82);
            this.Play_but.Name = "Play_but";
            this.Play_but.Size = new System.Drawing.Size(37, 23);
            this.Play_but.TabIndex = 10;
            this.Play_but.Text = "Play";
            this.Play_but.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Play_but.UseVisualStyleBackColor = true;
            this.Play_but.Click += new System.EventHandler(this.Play_but_Click);
            // 
            // Stop_but
            // 
            this.Stop_but.Location = new System.Drawing.Point(307, 111);
            this.Stop_but.Name = "Stop_but";
            this.Stop_but.Size = new System.Drawing.Size(37, 23);
            this.Stop_but.TabIndex = 11;
            this.Stop_but.Text = "Stop";
            this.Stop_but.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Stop_but.UseVisualStyleBackColor = true;
            this.Stop_but.Click += new System.EventHandler(this.Stop_but_Click);
            // 
            // Clear_sound_but
            // 
            this.Clear_sound_but.Location = new System.Drawing.Point(348, 95);
            this.Clear_sound_but.Name = "Clear_sound_but";
            this.Clear_sound_but.Size = new System.Drawing.Size(40, 23);
            this.Clear_sound_but.TabIndex = 12;
            this.Clear_sound_but.Text = "Clear";
            this.Clear_sound_but.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Clear_sound_but.UseVisualStyleBackColor = true;
            this.Clear_sound_but.Click += new System.EventHandler(this.Clear_sound_but_Click);
            // 
            // checkBox_telegram
            // 
            this.checkBox_telegram.AutoSize = true;
            this.checkBox_telegram.Checked = true;
            this.checkBox_telegram.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_telegram.Location = new System.Drawing.Point(118, 130);
            this.checkBox_telegram.Name = "checkBox_telegram";
            this.checkBox_telegram.Size = new System.Drawing.Size(70, 17);
            this.checkBox_telegram.TabIndex = 13;
            this.checkBox_telegram.Text = "Telegram";
            this.checkBox_telegram.UseVisualStyleBackColor = true;
            // 
            // checkBox_Sound
            // 
            this.checkBox_Sound.AutoSize = true;
            this.checkBox_Sound.Checked = true;
            this.checkBox_Sound.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_Sound.Location = new System.Drawing.Point(55, 130);
            this.checkBox_Sound.Name = "checkBox_Sound";
            this.checkBox_Sound.Size = new System.Drawing.Size(57, 17);
            this.checkBox_Sound.TabIndex = 14;
            this.checkBox_Sound.Text = "Sound";
            this.checkBox_Sound.UseVisualStyleBackColor = true;
            // 
            // EditOutlookConnectorForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 187);
            this.Controls.Add(this.checkBox_Sound);
            this.Controls.Add(this.checkBox_telegram);
            this.Controls.Add(this.Clear_sound_but);
            this.Controls.Add(this.Stop_but);
            this.Controls.Add(this.Play_but);
            this.Controls.Add(this.checkBox_enabled);
            this.Controls.Add(this.textBox_name);
            this.Controls.Add(this.Apply_but);
            this.Controls.Add(this.Browse_sound_but);
            this.Controls.Add(this.Browse_folder_but);
            this.Controls.Add(this.textBox_folder);
            this.Controls.Add(this.textBox_sound);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EditOutlookConnectorForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Edit Monitor";
            this.Load += new System.EventHandler(this.EditOutlookConnectorForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox_sound;
        private System.Windows.Forms.TextBox textBox_folder;
        private System.Windows.Forms.Button Browse_folder_but;
        private System.Windows.Forms.Button Browse_sound_but;
        private System.Windows.Forms.Button Apply_but;
        private System.Windows.Forms.TextBox textBox_name;
        private System.Windows.Forms.CheckBox checkBox_enabled;
        private System.Windows.Forms.Button Play_but;
        private System.Windows.Forms.Button Stop_but;
        private System.Windows.Forms.Button Clear_sound_but;
        private System.Windows.Forms.CheckBox checkBox_telegram;
        private System.Windows.Forms.CheckBox checkBox_Sound;
    }
}