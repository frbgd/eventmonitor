﻿using Microsoft.Office.Interop.Outlook;
using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace EventMonitor.Outlook
{
    //форма открытия папки Outlook
    public partial class SelectOutlookPathForm : Form
    {
        public SelectOutlookPathForm()
        {
            InitializeComponent();
        }

        private Microsoft.Office.Interop.Outlook.Application Application;
        private NameSpace NS;
        public string path { get; set; }

        private void SelectFolderForm_Load(object sender, EventArgs e)
        {
            if (Process.GetProcessesByName("OUTLOOK").Count() > 0)
            {
                try
                {
                    Application = Marshal.GetActiveObject("Outlook.Application") as Microsoft.Office.Interop.Outlook.Application;
                    NS = Application.GetNamespace("MAPI");
                    GetFolders();
                }
                catch (System.Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
                MessageBox.Show("Outlook isn't running!");
        }

        private void GetFolders()
        {
            foreach (Account account in NS.Accounts)
            {
                Folder root = (Folder)NS.Folders[account.DisplayName];
                EnumerateFolders(root);
            }
        }

        private void EnumerateFolders(Folder folder)
        {
            Folders childFolders = folder.Folders;
            treeView_folders.Nodes.Add(folder.FolderPath);
            if (childFolders.Count > 0)
            {
                foreach (Folder childFolder in childFolders)
                {
                    EnumerateFolders(childFolder);
                }
            }
        }

        private void treeView_folders_AfterCheck(object sender, TreeViewEventArgs e)
        {
            path = e.Node.Text;
            Close();
        }
    }
}
