﻿using System;
using System.Windows.Forms;
using WMPLib;

namespace EventMonitor.Outlook
{
    //класс формы редактирования монитора Outlook
    public partial class EditOutlookConnectorForm : EditConnectorForm
    {
        public EditOutlookConnectorForm()
        {
            InitializeComponent();
            Text = "New Monitor";
            monitor = new OutlookMonitor();
        }
        public EditOutlookConnectorForm(OutlookMonitor monitor)
        {
            InitializeComponent();
            this.monitor = monitor;
            textBox_name.Text = this.monitor.Name;
            textBox_folder.Text = this.monitor.Path;
            textBox_sound.Text = this.monitor.Sound;
            checkBox_enabled.Checked = this.monitor.Status;
            checkBox_telegram.Checked = this.monitor.tNotification;
            checkBox_Sound.Checked = this.monitor.sNotification;
        }

        private WindowsMediaPlayer wmp = new WindowsMediaPlayer();

        private void Browse_folder_but_Click(object sender, EventArgs e)
        {
            SelectOutlookPathForm selectFolderForm = new SelectOutlookPathForm();
            selectFolderForm.ShowDialog();
            if (selectFolderForm.path != null || selectFolderForm.path != "")
                textBox_folder.Text = selectFolderForm.path;
        }

        private void Browse_sound_but_Click(object sender, EventArgs e)
        {
            OpenFileDialog OFD = new OpenFileDialog();
            OFD.Multiselect = false;
            OFD.Filter = "(*.mp3)|*.mp3|(*.wav)|*.wav";
            if (OFD.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    textBox_sound.Text = OFD.FileName;
                }
                catch { }
            }
        }

        private void Apply_but_Click(object sender, EventArgs e)
        {
            if (textBox_folder.Text != "")
            {
                if (textBox_name.Text != "")
                    monitor.Name = textBox_name.Text;
                else
                {
                    MessageBox.Show("Name field can not be empty!");
                    monitor.Name = textBox_folder.Text;
                }
                monitor.Path = textBox_folder.Text;
                monitor.Sound = textBox_sound.Text;
                monitor.Status = checkBox_enabled.Checked;
                monitor.tNotification = checkBox_telegram.Checked;
                monitor.sNotification = checkBox_Sound.Checked;

                Close();

                isOk = true;
            }
            else
                MessageBox.Show("Folder fields can not be empty!");
        }

        private void Play_but_Click(object sender, EventArgs e)
        {
            wmp.URL = textBox_sound.Text;
        }

        private void Stop_but_Click(object sender, EventArgs e)
        {
            wmp.controls.stop();
        }

        private void EditOutlookConnectorForm_Load(object sender, EventArgs e)
        {
            wmp.controls.stop();
        }

        private void Clear_sound_but_Click(object sender, EventArgs e)
        {
            textBox_sound.Text = "";
        }
    }
}
