﻿using Microsoft.Office.Interop.Outlook;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;

namespace EventMonitor.Outlook
{
    //подключение к Outlook
    public class OutlookConnection : IConnection
    {
        [JsonIgnore]
        public Items Box;

        //проверка работы Outlook
        public bool Check()
        {
            if (Process.GetProcessesByName("OUTLOOK").Count() > 0)
            {
                Logger.AddLog("OutlookConnection: Microsoft Outlook is running");
                return true;
            }
            else
            {
                Logger.AddLog("OutlookConnection: Microsoft Outlook isn't running");
                return false;
            }
        }

        //проверка работы монитора
        public bool Check(string Path)
        {
            if (Process.GetProcessesByName("OUTLOOK").Count() > 0)
            {
                try
                {
                    return Initialization(Path);
                }
                catch
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        private bool Initialization(string Path)
        {
            try
            {
                Application Application = Marshal.GetActiveObject("Outlook.Application") as Application;

                NameSpace NS = Application.GetNamespace("MAPI");

                string[] SplitPath = Path.Split('\\');

                Folder folder = (Folder)NS.Folders[SplitPath[2]];

                for (int i = 3; i < SplitPath.Length; i++)
                {
                    folder = (Folder)folder.Folders[SplitPath[i]];
                }

                Box = folder.Items;

                Logger.AddLog($"OutlookConnection: Microsoft Outlook folder {Path} got");
                return true;
            }
            catch (System.Exception exception)
            {
                Logger.AddLog($"OutlookConnection: Microsoft Outlook folder {Path} didn't get. {exception.Message}");
                return false;
            }
        }
    }
}
