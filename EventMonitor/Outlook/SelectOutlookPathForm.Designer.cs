﻿namespace EventMonitor.Outlook
{
    partial class SelectOutlookPathForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SelectOutlookPathForm));
            this.treeView_folders = new System.Windows.Forms.TreeView();
            this.SuspendLayout();
            // 
            // treeView_folders
            // 
            this.treeView_folders.CheckBoxes = true;
            this.treeView_folders.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView_folders.Location = new System.Drawing.Point(0, 0);
            this.treeView_folders.Name = "treeView_folders";
            this.treeView_folders.Size = new System.Drawing.Size(584, 561);
            this.treeView_folders.TabIndex = 0;
            this.treeView_folders.AfterCheck += new System.Windows.Forms.TreeViewEventHandler(this.treeView_folders_AfterCheck);
            // 
            // SelectOutlookPathForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 561);
            this.Controls.Add(this.treeView_folders);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SelectOutlookPathForm";
            this.Text = "Select Path";
            this.Load += new System.EventHandler(this.SelectFolderForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView treeView_folders;
    }
}