﻿using MihaZupan;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot;

namespace EventMonitor
{
    //класс Телеграм socks5-прокси
    public class TelegramProxy
    { 
        public TelegramProxy(bool noProxy)
        {
            if (noProxy)
            {
                proxy = null;
                name = "No Proxy";
                isCurrent = true;
            }
            else
            {
                name = "New Proxy";
                isCurrent = false;
            }
        }

        public bool isCurrent { get; set; }
        public string name { get; set; }
        public string host { get; set; }
        public int port { get; set; }
        public string login { get; set; }
        [JsonIgnore]
        public string pass { get; set; }
        public string encryptedPass { get; set; }
        [JsonIgnore]
        private HttpToSocks5Proxy proxy { get; set; }

        public override string ToString()
        {
            return name;
        }

        public static bool operator ==(TelegramProxy tProxy1, TelegramProxy tProxy2)
        {
            if (tProxy1.proxy == tProxy2.proxy)
                return true;
            else
                return false;
        }

        public static bool operator !=(TelegramProxy tProxy1, TelegramProxy tProxy2)
        {
            if (tProxy1.proxy != tProxy2.proxy)
                return true;
            else
                return false;
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public void SetProxy()
        {
            if (!String.IsNullOrEmpty(host))
            {
                if (!String.IsNullOrEmpty(login) && !String.IsNullOrEmpty(pass))
                {
                    proxy = new HttpToSocks5Proxy(host, port, login, pass);
                    name = $"{login}@{host}:{port}";
                }
                else
                {
                    proxy = new HttpToSocks5Proxy(host, port);
                    name = $"{host}:{port}";
                }
                proxy.ResolveHostnamesLocally = true;
            }
        }
        public void SetProxy(string host, int port, string login = "", string pass = "")
        {
            this.host = host;
            this.port = port;
            this.login = login;
            this.pass = pass;
            if (!String.IsNullOrEmpty(this.login) && !String.IsNullOrEmpty(this.pass))
            {
                proxy = new HttpToSocks5Proxy(this.host, this.port, this.login, this.pass);
                name = $"{this.login}@{this.host}:{this.port}";
            }
            else
            {
                proxy = new HttpToSocks5Proxy(this.host, this.port);
                name = $"{this.host}:{this.port}";
            }
            proxy.ResolveHostnamesLocally = true;
        }

        public HttpToSocks5Proxy GetProxy()
        {
            return proxy;
        }
    }

    //класс оповещателя Телеграм
    public class TelegramNotifier
    {
        public TelegramNotifier(MainForm mainForm)
        {       
            proxies = new List<TelegramProxy>();
            this.mainForm = mainForm;
        }

        public bool status { get; set; } = false;
        public string botToken { get; set; } = null;
        public long chatId { get; set; } = 0;
        public List<TelegramProxy> proxies { get; set; }

        public string sound { get; set; }

        [JsonIgnore]
        public static TelegramBotClient botClient;
        [JsonIgnore]
        public MainForm mainForm;

        public async Task<bool> StartNotifier()
        {
            List<TelegramProxy> currentProxyList = (from i in proxies where i.isCurrent select i).ToList();
            if(currentProxyList.Count > 1)
            {
                foreach (TelegramProxy i in currentProxyList)
                    i.isCurrent = false;
                currentProxyList.First().isCurrent = true;
                currentProxyList = (from i in proxies where i.isCurrent select i).ToList();
            }
            try
            {
                if (!String.IsNullOrEmpty(currentProxyList.First().host))
                {
                    botClient = new TelegramBotClient(botToken, currentProxyList.First().GetProxy());
                    await botClient.GetMeAsync();
                }
                else
                {
                    botClient = new TelegramBotClient(botToken);
                    await botClient.GetMeAsync();
                }
            }
            catch
            {
                botClient = null;
                mainForm.changeTPic(false);
                status = false;
                Logger.AddLog($"ERROR: Failed to start Telegram notificator with parameters: Token: {botToken}, chatId: {chatId}, proxy: {(from proxy in proxies where proxy.isCurrent select proxy).ToList().First().name}, error sound: {sound}");
                return false;
            }
            mainForm.changeTPic(true);
            status = true;
            Logger.AddLog($"Telegram notificator started with parameters: Token: {botToken}, chatId: {chatId}, proxy: {(from proxy in proxies where proxy.isCurrent select proxy).ToList().First().name}, error sound: {sound}");
            return true;
        }

        public bool StopNotifier()
        {
            try
            {
                botClient = null;
            }
            catch
            {
                mainForm.changeTPic(true);
                status = true;
                Logger.AddLog("ERROR: Failed to stop Telegram notificator");
                return false;
            }
            mainForm.changeTPic(false);
            status = false;
            Logger.AddLog("Telegram notificator stopped");
            return true;
        }

        public async Task<bool> Notify(string title, string text)
        {
            try
            {
                await botClient.SendTextMessageAsync(chatId, text);
            }
            catch
            {
                Logger.AddLog($"ERROR: Failed to send Telegram message. About {title}");
                return false;
            }
            Logger.AddLog($"Telegram message successfully  sent. About {title}");
            return true;
        }
    }
}
