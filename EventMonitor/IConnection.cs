﻿namespace EventMonitor
{
    //интерфейс подключений - подразумевается, что в дальнейшем программа будет работать не только с Outlook
    public interface IConnection
    {
        bool Check();
        bool Check(string Path);
    }
}
