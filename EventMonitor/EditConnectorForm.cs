﻿using System.Windows.Forms;

namespace EventMonitor
{
    //базовый класс формы редактирования монитора
    public class EditConnectorForm : Form
    {
        public Monitor monitor { get; set; }
        public bool isOk { get; set; } = false;
    }
}
