﻿using System;
using System.IO;

namespace EventMonitor
{
    //статический логгер
    static class Logger
    {
        static object locker = new object();
        public static void AddLog(string str_log)
        {
            lock (locker)
            {
                if (File.Exists("event_monitor.log") == false)
                    File.AppendAllText("event_monitor.log", "");

                File.AppendAllText("event_monitor.log", $"\r\n\r\n{DateTime.Now.ToString()} {str_log}");
            }
        }
    }
}
