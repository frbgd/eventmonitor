﻿namespace EventMonitor
{
    partial class EditConnMonForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditConnMonForm));
            this.Stop_but = new System.Windows.Forms.Button();
            this.Play_but = new System.Windows.Forms.Button();
            this.Apply_but = new System.Windows.Forms.Button();
            this.Browse_sound_but = new System.Windows.Forms.Button();
            this.textBox_sound = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.numericUpDown_delay = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.Clear_but = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_delay)).BeginInit();
            this.SuspendLayout();
            // 
            // Stop_but
            // 
            this.Stop_but.Location = new System.Drawing.Point(303, 91);
            this.Stop_but.Name = "Stop_but";
            this.Stop_but.Size = new System.Drawing.Size(37, 23);
            this.Stop_but.TabIndex = 20;
            this.Stop_but.Text = "Stop";
            this.Stop_but.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Stop_but.UseVisualStyleBackColor = true;
            this.Stop_but.Click += new System.EventHandler(this.Stop_but_Click);
            // 
            // Play_but
            // 
            this.Play_but.Location = new System.Drawing.Point(303, 48);
            this.Play_but.Name = "Play_but";
            this.Play_but.Size = new System.Drawing.Size(37, 23);
            this.Play_but.TabIndex = 19;
            this.Play_but.Text = "Play";
            this.Play_but.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Play_but.UseVisualStyleBackColor = true;
            this.Play_but.Click += new System.EventHandler(this.Play_but_Click);
            // 
            // Apply_but
            // 
            this.Apply_but.Location = new System.Drawing.Point(212, 106);
            this.Apply_but.Name = "Apply_but";
            this.Apply_but.Size = new System.Drawing.Size(75, 23);
            this.Apply_but.TabIndex = 18;
            this.Apply_but.Text = "Ok";
            this.Apply_but.UseVisualStyleBackColor = true;
            this.Apply_but.Click += new System.EventHandler(this.Apply_but_Click);
            // 
            // Browse_sound_but
            // 
            this.Browse_sound_but.Location = new System.Drawing.Point(402, 71);
            this.Browse_sound_but.Name = "Browse_sound_but";
            this.Browse_sound_but.Size = new System.Drawing.Size(70, 23);
            this.Browse_sound_but.TabIndex = 17;
            this.Browse_sound_but.Text = "Browse...";
            this.Browse_sound_but.UseVisualStyleBackColor = true;
            this.Browse_sound_but.Click += new System.EventHandler(this.Browse_sound_but_Click);
            // 
            // textBox_sound
            // 
            this.textBox_sound.Location = new System.Drawing.Point(57, 71);
            this.textBox_sound.Name = "textBox_sound";
            this.textBox_sound.ReadOnly = true;
            this.textBox_sound.Size = new System.Drawing.Size(240, 20);
            this.textBox_sound.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Sound";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Period";
            // 
            // numericUpDown_delay
            // 
            this.numericUpDown_delay.Location = new System.Drawing.Point(57, 36);
            this.numericUpDown_delay.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.numericUpDown_delay.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numericUpDown_delay.Name = "numericUpDown_delay";
            this.numericUpDown_delay.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown_delay.TabIndex = 21;
            this.numericUpDown_delay.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(183, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 22;
            this.label1.Text = "seconds";
            // 
            // Clear_but
            // 
            this.Clear_but.Location = new System.Drawing.Point(347, 71);
            this.Clear_but.Name = "Clear_but";
            this.Clear_but.Size = new System.Drawing.Size(40, 23);
            this.Clear_but.TabIndex = 23;
            this.Clear_but.Text = "Clear";
            this.Clear_but.UseVisualStyleBackColor = true;
            this.Clear_but.Click += new System.EventHandler(this.Clear_but_Click);
            // 
            // EditConnMonForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 161);
            this.Controls.Add(this.Clear_but);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.numericUpDown_delay);
            this.Controls.Add(this.Stop_but);
            this.Controls.Add(this.Play_but);
            this.Controls.Add(this.Apply_but);
            this.Controls.Add(this.Browse_sound_but);
            this.Controls.Add(this.textBox_sound);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "EditConnMonForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Connection Monitoring Parameters";
            this.Load += new System.EventHandler(this.EditConnMonForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_delay)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Stop_but;
        private System.Windows.Forms.Button Play_but;
        private System.Windows.Forms.Button Apply_but;
        private System.Windows.Forms.Button Browse_sound_but;
        private System.Windows.Forms.TextBox textBox_sound;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numericUpDown_delay;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Clear_but;
    }
}